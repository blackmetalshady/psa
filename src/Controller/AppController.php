<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $loggedIn = $this->Auth->user();
        //print_r($loggedIn); die();
        $this->loadModel('Rdvs');
        if ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0e') { //Si Etudiant
            $cpt = $this->Rdvs->find()->where(['student_id'=>$this->Auth->user()['id'], 'student_accept'=>'0', 'cancelled'=>'0'])->count();
            $rdvaction = 'rdvstudent';
        }else if ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0c'){ //Si tuteur entreprise
            $cpt = $this->Rdvs->find()->where(['extutor_id'=>$this->Auth->user()['id'], 'tutor_accept'=>'0', 'cancelled'=>'0'])->count();
            $rdvaction = 'rdvstudent';
        }else{ //Si Tuteur Pédagogique
            $cpt = $this->Rdvs->find()->where(['inttutor_id'=>$this->Auth->user()['id'], 'student_accept'=>'0','tutor_accept'=>'0', 'cancelled'=>'0'])->count();
            $rdvaction = 'rdv';
        }
        $this->set(compact('loggedIn','cpt','rdvaction'));
        //$this->set('loggedIn', $loggedIn);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'logoutRedirect' => '/',
            'loginRedirect' => '/',
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

     public function forgotpasswordemail($user, $token){

        $url = Router::url([
                '_full'=>true,
                '_name' => 'resetpassword' //configured in Routes
            ]);
        $url .= $user->id.'/'.$token;
        $fullname = ucfirst($user->individual->title).'. '.ucfirst($user->individual->first_name).' '.ucfirst($user->individual->last_name);

        /*echo $fullname.'<br>';*/
        //echo $url;die();
        $email = new Email();
        $email->template('forgotpassword', 'default');
        $email->viewVars(['link' => $url,'fullname'=>$fullname]);
        $email->from(['info@ramcarloc.com' => 'PSA'])
            ->emailFormat('html')
            ->to($user->email)
            ->subject('PSA: '.__('Veuillez réinitialiser votre mot de passe'));
        if ($email->send()) {
            return true;
        }else{
            return false;
        }
    }
}
