<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 *
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Users', 'ParentMessages']
        ];
        $messages = $this->paginate($this->Messages);

        $this->set(compact('messages'));*/
        $messages = $this->Messages
                         ->find()
                         ->where(function($exp){
                            return $exp->or_([
                                'Messages.to'=>$this->request->getSession()->read('Auth.User.id'),
                                'Messages.user_id'=>$this->request->getSession()->read('Auth.User.id')
                            ]);
                         })
                         //->orWhere(['Messages.to'=>$this->request->getSession()->read('Auth.User.id')]) ['Messages.user_id'=>$this->request->getSession()->read('Auth.User.id')]
                         ->andWhere(['Messages.parent_id'=>'NULL'])
                         ->order(['Messages.sendtime']);
        $this->set(compact('messages'));
    }

    public function inbox(){

    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => ['Users', 'ParentMessages', 'ChildMessages']
        ]);

        $this->set('message', $message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        //print_r($this->request->getData()); die();

        if ($this->request->is('post')) {

            if (!empty($this->request->getData()['rapport']['tmp_name'])) {
                $ext = strtolower(pathinfo($this->request->getData()['rapport']['name'], PATHINFO_EXTENSION));
                $filename = $this->request->getData()['rapport']['name'].'_'.time().'.'.$ext;
                $finalname = WWW_ROOT.'files/'.$filename;
                if(move_uploaded_file($this->request->getData()['rapport']['tmp_name'], $finalname)){
                    chmod($finalname, 0766);
                    $data['filename'] = $filename;
                }else{
                    echo "Erreur upload"; die();
                }
            }

            if (empty($this->request->getData()['conversation_id'])) {
                $this->loadModel('Conversations');
                $conv = $this->Conversations->newEntity();
                $data = $this->request->getData();
                $data['user_id'] = $this->Auth->user()['id'];
                //print_r($data); die();
                $this->Conversations->patchEntity($conv, $data);
                if (!$conversation = $this->Conversations->save($conv)) {
                    print_r($conv); die();
                }
            }

            //print_r($conversation); die();

            if (!empty($this->request->getData()['to_user'])) {
                $message = $this->Messages->newEntity();
                $data['conversation_id'] = $conversation['id'];
                //print_r($data); die();
                //array_push($this->request->getData(), $this->Auth->user()['id']);
                //print_r($this->Auth->user()); die();
                $message = $this->Messages->patchEntity($message, $data);
                //print_r($message); die();
                if ($this->Messages->save($message)) {
                    return $this->redirect(['controller'=>'Users', 'action'=>'inbox']);
                }else{
                    print_r($message);die();
                }
            }else{
                $this->loadModel('Users');
                $tutors = $this->Users->get($this->Auth->user()['id'], ['contain'=>['Tutors']]);

                foreach ($tutors->tutors as $tutor) {
                    $message = $this->Messages->newEntity();
                    $data['conversation_id'] = $conversation['id'];
                    //print_r($data); die();
                    //array_push($this->request->getData(), $this->Auth->user()['id']);
                    //print_r($this->Auth->user()); die();
                    $data['to_user'] = $tutor->id;
                    $message = $this->Messages->patchEntity($message, $data);
                    //print_r($message); die();
                    if (!$this->Messages->save($message)) {
                        print_r($message);die();
                    }
                }
                return $this->redirect(['controller'=>'Users', 'action'=>'inbox']);
            }
        }
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $parentMessages = $this->Messages->ParentMessages->find('list', ['limit' => 200]);
        $this->set(compact('message', 'users', 'parentMessages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The message could not be saved. Please, try again.'));
        }
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $parentMessages = $this->Messages->ParentMessages->find('list', ['limit' => 200]);
        $this->set(compact('message', 'users', 'parentMessages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function conversation($id = null){
        $this->loadModel('Conversations');
        $conversation = $this->Conversations->get($id, ['contain'=>[
                                                'Users',
                                                'Messages'=>[
                                                    'Users',
                                                    'sort'=>['Messages.sendtime'=>'DESC']
                                                ]
                                            ]
                                        ]);
        $this->set(compact('conversation'));
    }

    public function addtoconversation(){
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user()['id'];

            if (!empty($this->request->getData()['rapport']['tmp_name'])) {
                $ext = strtolower(pathinfo($this->request->getData()['rapport']['name'], PATHINFO_EXTENSION));
                $filename = $this->request->getData()['rapport']['name'].'_'.time().'.'.$ext;
                $finalname = WWW_ROOT.'files/'.$filename;
                if(move_uploaded_file($this->request->getData()['rapport']['tmp_name'], $finalname)){
                    chmod($finalname, 0766);
                    $data['filename'] = $filename;
                }else{
                    echo "Erreur upload"; die();
                }
            }

                $message = $this->Messages->newEntity();
                $message = $this->Messages->patchEntity($message, $data);

                if ($this->Messages->save($message)) {
                    return $this->redirect(['controller'=>'Messages', 'action'=>'conversation', $data['conversation_id']]);
                }else{
                    print_r($message);die();
                }

        }
    }


}
