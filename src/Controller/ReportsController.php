<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 *
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportsController extends AppController
{

    public function add()
    {
        $report = $this->Reports->newEntity();
        if ($this->request->is('post')) {
            if (!empty($this->request->getData()['rapport']['tmp_name'])) {
                $ext = strtolower(pathinfo($this->request->getData()['rapport']['name'], PATHINFO_EXTENSION));
                $filename = $this->request->getData()['rapport']['name'].'_'.time().'.'.$ext;
                $finalname = WWW_ROOT.'files/'.$filename;
                if(move_uploaded_file($this->request->getData()['rapport']['tmp_name'], $finalname)){
                    chmod($finalname, 0766);
                }else{
                    echo "Erreur upload"; die();
                }
            }
            $data = $this->request->getData();
            if (empty($data['inttutor_id'])) {
                $data['inttutor_id'] = $this->Auth->user()['id'];
            }
            $data['filename'] = $filename;
            $report = $this->Reports->patchEntity($report, $data);
            if ($rprt = $this->Reports->save($report)) {

                $this->loadModel('Rdvs');
                $rdv = $this->Rdvs->get($data['rdv_id']);
                $rdv = $this->Rdvs->patchEntity($rdv, ['report_id'=>$rprt['id']]);
                //print_r($rdv ); die();
                if ($this->Rdvs->save($rdv)) {
                    return $this->redirect(Router::url( $this->referer(), true ));
                }
                print_r($rdv); die();
            }
            print_r($report); die();
        }

    }

    public function delete($id='')
    {
        $this->request->allowMethod(['post', 'delete']);
        $report = $this->Reports->get($id);
        $this->loadModel('Rdvs');
        if (!$this->Reports->delete($report)) {
            print_r($report); die();
        } 
        return $this->redirect(Router::url( $this->referer(), true ));
    }
}
