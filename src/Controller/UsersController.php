<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Routing\Router;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles', 'Institutions']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Institutions']
        ]);

        $this->set('user', $user);
    }

    public function login()
    {
        //echo $loggedIn; die();
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                if ($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0a'){ //Admin Pole
                    return $this->redirect(['prefix'=>'admin', 'controller'=>'Users','action'=>'formations']);
                }elseif($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0b' || $user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0f'){ //Admin de l'univérsité ou admin de Formation
                    return $this->redirect(['prefix'=>'admin', 'controller'=>'Users','action'=>'index']);
                }elseif ($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0c'){ //tuteur entreprise 
                    return $this->redirect('/');
                }elseif ($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0d') { // tuteur pedagogique
                    return $this->redirect('/');
                }else{ // etudiant
                    return $this->redirect('/');
                }
            }else{
                $msg = 'Utilisateur ou Mot de passe Incorrecte';
            }
            $this->set(compact('msg'));
            //$this->viewBuilder()->layout(false);
            //$this->Flash->error(__(''));
        }

        $this->viewBuilder()->setLayout(false);
        
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $institutions = $this->Users->Institutions->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'institutions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $institutions = $this->Users->Institutions->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'institutions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

        private function register($data){
        $data['registrationtoken'] = bin2hex(openssl_random_pseudo_bytes(16));
        if ($this->Users->exists(['email'=>$data['email']])) {
            $this->Flash->error(__('The email you entred already exists'));
            return false;
        }else{
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user,$data);
            if ($result = $this->Users->save($user)) {
                $this->Flash->success(__('Vous vous etes inscrit avec succes, votre compte sera accessible dès qu\'un administrateur le valide'));
                return true;
            } else {
                $this->Flash->error(__('An Error Occured, please try again later'));
                return false;
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */


        public function forgotpassword(){

        if ($this->request->is('post')) {
            //Get user by Email
            $user = $this->Users->find()
                                ->where(['email'=>$this->request->data['email']])
                                ->first();
            //If the user exists, proceed with token generation
            if (!empty($user)) {
                //generating the token based on hashed password concatinated with the current timestamp
                $token = bin2hex(openssl_random_pseudo_bytes(16));

                //all other tokens shall be expired, updating all rows for the user
                $this->loadModel('Passwordtokens');
                $update = $this->Passwordtokens->query()->update()
                                     ->set(['expired'=>'1'])
                                     ->where(['user_id'=>$user->id]);
                //if the update was successfull, proceed
                if ($update->execute()) {
                    $ptoken = $this->Passwordtokens->newEntity();
                    $ptoken = $this->Passwordtokens->patchEntity($ptoken, [
                                                                    'token'=>$token,
                                                                    'user_id'=>$user->id,
                                                                    'start_time'=>Time::now()
                                                                ]);
                    //Saving the new token
                    if ($this->Passwordtokens->save($ptoken)) {
                        //Sending the RESET link to the user
                        //$appemails = new AppEmails;
                        if($this->forgotpasswordemail($user, $token)){
                            //Everything is ok
                            $this->Flash->success(__('Un email de réinitialisation de mot de passe à été envoyé a votre adresse'));
                        }else{

                            $this->Flash->error(__('Une erreur s\'est produit, veuillez réessayer'));
                        }
                    //if the save of the token went wrong
                    }else{
                        print_r($ptoken); die();
                        //"An Error Occured, please Try Again Later";
                        $this->Flash->error(__('Une erreur s\'est produit, veuillez réessayer'));
                    }
                //if updating token as expired went wrong
                }else{
                    //"An Error Occured, please Try Again Later";
                }
            //if the user was not found in the database
            }else{
                //"No user with the email '".$user->email."' has been found in our database";
                $this->Flash->error(__('veuillez verifier votre email'));
            }
        }
        $forgotpassword = true;
        $this->set(compact('forgotpassword'));
        $this->render('login');
    }
//**************************************************************************//
//**************************************************************************//
    public function resetpassword($id, $tokenstring){
        if (!empty($id) && !empty($tokenstring)) {
            $this->loadModel('Passwordtokens');
            $token = $this->Passwordtokens->find()->where([
                                                'user_id'=>$id,
                                                'token'=>$tokenstring
                                                ])
                                         ->first();

            if (!empty($token) && $token->expired == '0') {
                $periodinseconds = 15*60;
                $tokentime = $token->start_time->toUnixString();
                $now = Time::now()->toUnixString();
                $diff = $now - $tokentime;
                if ($diff > $periodinseconds) {
                    throw new NotFoundException();
                }else{
                    $this->request->session()->write('ForgotPassword.token',$token);
                }
            }else{
                throw new NotFoundException();
            }
        }else{
            throw new NotFoundException();
        }
        $this->viewBuilder()->layout('login');
    }

    public function changeforgotenpassword(){
        $token = $this->request->session()->read('ForgotPassword.token');
        
        if (!empty($token)) {
            $passwordhasher = new DefaultPasswordHasher;
            if (strlen($this->request->data['password']) < 6) {
                $this->Flash->error(__('Le Mot de passe doit etre d\'au moins 6 characteres'));
                //echo "lengtherror";die();
            }else if ($this->request->data['password'] != $this->request->data['confirm_password']) {
                $this->Flash->error(__('Le Mot de passe et sa confirmation sont different'));
                //echo "newpassnomatch";die();
            }else{
                $user = $this->Users->get($token->user_id);
                $user = $this->Users->patchEntity($user,$this->request->data);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Mot de passe réinitialisé avec succes, veuillez vous authentifier'));

                        // set TokenPassword as Expired
                        $this->loadModel('Passwordtokens');
                        $token = $this->Passwordtokens->patchEntity($token,['expired'=>'1']);
                        $this->Passwordtokens->save($token);
                        //deleting the token from session
                        $this->request->session()->delete('ForgotPassword.token');
                        
                        return $this->redirect(['action' => 'login']);
                    }else{
                        $this->Flash->error(__('Erreur lors de la réinitialisation du mot de passe, veuillez reessayer'));
                        return $this->redirect(Router::url( $this->referer(), true ));
                    }
            }
        }else{
            return $this->redirect(['action' => 'login']);
        }
    }
//**************************************************************************//
    public function forum(){
        $user = $this->Auth->user();

        $this->set(compact('user'));
    }

    public function inbox(){
        $this->loadModel('Conversations');
        $userid = $this->Auth->user()['id'];

        
        /*$messages = $this->Messages->find()->select(['id'])
                                   ->where(function($exp){
                                        return $exp->or_([
                                            'Messages.user_id'=>$this->Auth->user()['id'],
                                            'Messages.to_user'=>$this->Auth->user()['id']
                                        ]);
                                    })
                                   /*->andWhere(function ($exp) {
                                        return $exp->isNull('Messages.parent_id');
                                    })
                                   //->contain(['Conversations'])
                                   ->order(['Messages.sendtime'=>'DESC']);*/
        $conv = $this->Conversations->find()->contain('Messages', function ($q) {
                                               return $q
                                                    ->where(function($exp){
                                                        return $exp->or_([
                                                            'Messages.user_id'=>$this->Auth->user()['id'],
                                                            'Messages.to_user'=>$this->Auth->user()['id']
                                                        ]);
                                                    });
                                            })
                                        ->order(['conv_time'=>'DESC']);
        //print_r($conv->toArray()); die();                           
        $this->set(compact('conv','userid'));
    }

    public function newmessage(){
        $user = $this->Users->find()->where(['id'=>$this->Auth->user()['id']])
                                            ->contain(['Tutors'=>['Roles']])->first();
        $this->set(compact('user'));
    }


    public function studenttutors()
    {
        if ($this->request->is('post')) {
            $tutors = $this->Users->find()->where(['id'=>$this->request->getData()['student_id']])
                                          ->contain(['Tutors'=>function($q){
                                                return $q->select(['id','firstname','lastname'])
                                                         ->where(['role_id !='=>'0646b17f-edae-426d-8235-3bbbb0240d0d']);
                                            }])->first()['tutors'];
            echo json_encode($tutors); die();
        }
    }

    public function rdv(){
        $this->loadModel('Rdvs');
        $students = $this->Users->get($this->Auth->user()['id'], [
            'contain'=>[
                'Students'=>function($q){
                    return $q->where(['current_tutor'=>'1']);
                }
            ]
        ]);
        $rdvs = $this->Rdvs->find()->where(['inttutor_id'=>$this->Auth->user()['id']])->contain(['Students','ExtTutors'])->order(['rdv_date'=>'DESC']);
        $students = $students->students;
        $this->set(compact('students','rdv','rdvs'));
    }

    public function rdvstudent(){
        $this->loadModel('Rdvs');
        $rdvs = $this->Rdvs->find();
        if ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0e') {
            $rdvs->where(['student_id'=>$this->Auth->user()['id']])->contain(['IntTutors','ExtTutors'])->order(['rdv_date'=>'DESC']);
        }elseif ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0c') {
            $rdvs->where(['extutor_id'=>$this->Auth->user()['id']])->contain(['IntTutors','Students'])->order(['rdv_date'=>'DESC']);
        }
        
        $key = ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0e')? 'student_accept' : 'tutor_accept';
         //print_r($rdvs->toArray()); die();
        $this->set(compact('rdvs','key'));
    }

    public function rdvadd()
    {
        if($this->request->is('post')){
            $this->loadModel('Rdvs');
            $rdv = $this->Rdvs->newEntity();
            $data = $this->request->getData();
            $data['inttutor_id'] = $this->Auth->user()['id'];
            $rdv = $this->Rdvs->patchEntity($rdv, $data);

            if ($this->Rdvs->save($rdv)) {
                return $this->redirect(Router::url($this->referer(), true));
            }

            print_r($rdv); die();
        }
    }

    public function rdvactions($id, $action){
        $key = ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0e')? 'student_accept' : 'tutor_accept';
        if (!empty($id) && !empty($action)) {
            $this->loadModel('Rdvs');
            $rdv = $this->Rdvs->find()->where(['id'=>$id])->first();
            
            if ($action == 'refuse') {
                $rdv = $this->Rdvs->patchEntity($rdv,[$key=>'2']);
            }else if ($action == 'accept'){
                $rdv = $this->Rdvs->patchEntity($rdv,[$key=>'1']);
            }else{
                echo "Unknown Action"; die();
            }
            //print_r($rdv); die();
            if (!$this->Rdvs->save($rdv)) {
                   print_r($rdv); die();
            }
        }
        return $this->redirect(Router::url($this->referer(), true));
    }

    public function rdvupdate(){
        if($this->request->is('post')){
            $this->loadModel('Rdvs');
            $rdv = $this->Rdvs->get($this->request->getData()['id']);
            $rdv = $this->Rdvs->patchEntity($rdv, $this->request->getData());
            if ($this->Rdvs->save($rdv)) {
                return $this->redirect(Router::url( $this->referer(), true ));
            }

            print_r($rdv); die();
        }

    }

    public function rdvcancel($id)
    {
        //print_r($id); die();
        if (!empty($id)) {
            $this->loadModel('Rdvs');
            $rdv = $this->Rdvs->get($id);
            $rdv = $this->Rdvs->patchEntity($rdv, ['cancelled'=>'1']);
            //print_r($rdv); die();
            if ($this->Rdvs->save($rdv)) {
                return $this->redirect(Router::url( $this->referer(), true ));
            }

            print_r($rdv); die();
        }
        return $this->redirect(Router::url( $this->referer(), true ));
    }

    public function reports(){
        $this->loadModel('Reports');
        $reports = $this->Reports->find()
                                 ->contain(['Students','ExtTutors','IntTutors','Rdvs'])
                                 ->order(['upload_time'=>'DESC']);
        //print_r($reports->toArray()); die();
        $students = $this->Users->get($this->Auth->user()['id'], ['contain'=>['Students']]);
        $this->set(compact('reports','students'));
    }

    public function getexttutors(){
        $id = $this->request->getData()['student_id'];
        $this->loadModel('Users');
        $usr = $this->Users->get($id, ['contain'=>['Tutors']]);
        //print_r($usr); die();
        $options = "<option value=''>--</option>";
        foreach ($usr->tutors as $t) {
            if ($t->role_id == "0646b17f-edae-426d-8235-3bbbb0240d0c") {
                $options .= "<option value='".$t->id."'>".$t->firstname." ".$t->lastname."</option>";
            }
        }

        echo $options;die();
    }

    public function getrdvs()
    {
        if ($this->request->is('post')) {
            $data = [
            'student_id'=>$this->request->getData()['student_id'],
            'extutor_id'=>$this->request->getData()['exttutor_id'],
            'inttutor_id' => $this->Auth->user()['id'],
            'cancelled' => '0',
            'student_accept' => '1',
            'tutor_accept' => '1',
            'report_id IS NULL'];
            $rdvs = $this->Rdvs->find()->where($data)->order(['rdv_date'=>'DESC']);
            $options = "<option value=''>--</option>";
            foreach ($rdvs as $rdv) {
                $options .= "<option value='".$rdv->id."'>".$rdv->rdv_date->i18nFormat('dd/MM/yyyy')." ".$rdv->rdv_time->i18nFormat('HH:mm')."</option>\n";
            }

            echo $options; die();
        }
        
    }
}
