<?php
namespace App\Controller;
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Routing\Router;

/**
 * Poles Controller
 *
 * @property \App\Model\Table\PolesTable $Poles
 *
 * @method \App\Model\Entity\Pole[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PolesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Users');
        $this->loadModel('Formations');

        $poles = $this->Poles->find()->contain(['Users','Formations'])->order(['name'=>'ASC']);
        $formations = $this->Formations->find()->order(['shortname'=>'ASC'])->combine('id', 'fullname');
        $title = "Gestion des Poles";
        $admins = $this->Users->find()
                              ->where(['role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0a'])
                              ->map(function ($row) {
                                    $row->fullname = $row->firstname.' '.$row->lastname;
                                    return $row;
                                })
                              ->combine('id', 'fullname');
        //print_r($admins->toArray()); die();
        $this->set(compact('poles','title','admins','formations'));
    }

    public function adminaffect()
    {

        $poles = $this->Poles->find()->contain(['Users']);
        $title = "Gestion des Poles";
        $this->loadModel('Users');
        $admins = $this->Users->find()
                              ->where(['role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0a'])
                              ->map(function ($row) {
                                    $row->fullname = $row->firstname.' '.$row->lastname;
                                    return $row;
                                })
                              ->combine('id', 'fullname');
        //print_r($admins->toArray()); die();
        $this->set(compact('poles','title','admins'));
    }
    /**
     * View method
     *
     * @param string|null $id Pole id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pole = $this->Poles->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('pole', $pole);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pole = $this->Poles->newEntity();
        if ($this->request->is('post')) {
            $pole = $this->Poles->patchEntity($pole, $this->request->getData());
            if ($this->Poles->save($pole)) {
                return $this->redirect(Router::url( $this->referer(), true ));
            }
            print_r($pole); die();
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Pole id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        //print_r($this->request->getData()); die();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('Formations');
            $data = $this->request->getData();
            //print_r($data);die();
            //$id = $this->request->getData()['id'];
            $pole = $this->Poles->get($data['id'], [
                'contain' => ['Formations']
            ]);
            //print_r($pole); die();
            if (!empty($data['formations'])) {
                $formations =[];
                for ($i=0; $i < sizeof($data['formations']); $i++) { 
                    $formations[] = ['id'=>$data['formations'][$i]];
                }
                $data['formations'] = $formations;
            }
            
            $pole = $this->Poles->patchEntity($pole, $data);
            //print_r($pole); die();
            if (!$this->Poles->save($pole)) {
                print_r($pole); die();
            }
        }
        return $this->redirect(Router::url( $this->referer(), true ));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pole id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pole = $this->Poles->get($id);
        if (!$this->Poles->delete($pole)) {
            print_r($pole); die();
        }

        return $this->redirect(Router::url( $this->referer(), true ));
    }
}
