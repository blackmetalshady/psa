<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'logoutRedirect' => [
                'prefix'=>false,
                'controller' => 'Pages',
                'action' => 'index'
            ],
            'authorize' => 'Controller'
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $loggedIn = $this->Auth->user();

        $this->set(compact('loggedIn'));
    }
    
    public function isAuthorized($user = null)
        {
            $stat = false;
            // Any registered user can access public functions
            if (empty($this->request->getParam('prefix'))) {
                $stat = true;
                return true;
            }

            // Only admins can access admin functions
            if ($this->request->getParam('prefix') === 'admin') {
                return (bool)($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0a' || $user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0b' || $user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0f');
            }
            //echo $stat; die();
            // Default deny
            return false;
        }
}