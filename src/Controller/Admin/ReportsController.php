<?php
namespace App\Controller;
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Routing\Router;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 *
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tutors', 'Students']
        ];
        $reports = $this->paginate($this->Reports);

        $this->set(compact('reports'));
    }

    /**
     * View method
     *
     * @param string|null $id Report id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $report = $this->Reports->get($id, [
            'contain' => ['Tutors', 'Students']
        ]);

        $this->set('report', $report);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $report = $this->Reports->newEntity();
        if ($this->request->is('post')) {
            if (!empty($this->request->getData()['rapport']['tmp_name'])) {
                $ext = strtolower(pathinfo($this->request->getData()['rapport']['name'], PATHINFO_EXTENSION));
                $filename = $this->request->getData()['rapport']['name'].'_'.time().'.'.$ext;
                $finalname = WWW_ROOT.'files/'.$filename;
                if(move_uploaded_file($this->request->getData()['rapport']['tmp_name'], $finalname)){
                    chmod($finalname, 0766);
                }else{
                    echo "Erreur upload"; die();
                }
            }
            $data = $this->request->getData();
            if (empty($data['inttutor_id'])) {
                $data['inttutor_id'] = $this->Auth->user()['id'];
            }
            $data['filename'] = $filename;
            $report = $this->Reports->patchEntity($report, $data);
            if ($rprt = $this->Reports->save($report)) {

                $this->loadModel('Rdvs');
                $rdv = $this->Rdvs->get($data['rdv_id']);
                $rdv = $this->Rdvs->patchEntity($rdv, ['report_id'=>$rprt['id']]);
                //print_r($rdv ); die();
                if ($this->Rdvs->save($rdv)) {
                    return $this->redirect(Router::url( $this->referer(), true ));
                }
                print_r($rdv); die();
            }
            print_r($report); die();
        }

    }

    /**
     * Edit method
     *
     * @param string|null $id Report id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $report = $this->Reports->get($this->request->getData()['id']);
            $data = $this->request->getData();

            if (!empty($this->request->getData()['rapport']['tmp_name'])) {
                $ext = strtolower(pathinfo($this->request->getData()['rapport']['name'], PATHINFO_EXTENSION));
                $filename = 'Rapport_'.time().'.'.$ext;
                $finalname = WWW_ROOT.'files/'.$filename;
                if(move_uploaded_file($this->request->getData()['rapport']['tmp_name'], $finalname)){
                    chmod($finalname, 0766);
                    $data['filename'] = $filename;
                }else{
                    echo "Erreur upload"; die();
                }
            }
            
            $report = $this->Reports->patchEntity($report, $data);
            if ($this->Reports->save($report)) {
                return $this->redirect(Router::url( $this->referer(), true ));
            }
            print_r($report); die();
        }

    }

    /**
     * Delete method
     *
     * @param string|null $id Report id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Rdvs');
        $report = $this->Reports->get($id);
        $rdv = $this->Rdvs->find()->where(['report_id'=>$report->id])->first();
        //print_r($report);die();
        $rdv = $this->Rdvs->patchEntity($rdv, ['report_id'=>null]);

        if ($this->Rdvs->save($rdv)) {
            $this->Reports->delete($report);
            return $this->redirect(Router::url( $this->referer(), true ));
        }
        print_r($rdv); die();
    }

    public function getrdvs()
    {
        if ($this->request->is('post')) {
            $this->loadModel('Rdvs');
            $rdvs = $this->Rdvs->find()->where(['inttutor_id'=>$this->request->getData()['tutor_id'], 'cancelled'=>'0', 'rdv_date <= CURDATE()', 'report_id IS NULL'])->order(['rdv_date'=>'DESC']);
            $options = "<option value=''>--</option>\n";
            foreach($rdvs as $rdv){
                $options .= "<option value='".$rdv->id."'>".$rdv->rdv_date->i18nFormat('dd/MM/yyyy')." ".$rdv->rdv_time->i18nFormat('HH:mm')."</option>\n";
            }

            echo $options; die();
        }
    }

    public function getexttutors(){
        $id = $this->request->getData()['student_id'];
        $this->loadModel('Users');
        $usr = $this->Users->get($id, ['contain'=>['Tutors']]);
        //print_r($usr); die();
        $options = "<option value=''>--</option>";
        foreach ($usr->tutors as $t) {
            if ($t->role_id == "0646b17f-edae-426d-8235-3bbbb0240d0c") {
                $options .= "<option value='".$t->id."'>".$t->firstname." ".$t->lastname."</option>";
            }
        }

        echo $options;die();
    }
}
