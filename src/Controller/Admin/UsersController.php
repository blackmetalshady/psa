<?php
namespace App\Controller;
namespace App\Controller\Admin;

use Cake\Event\Event;
use App\Controller\Admin\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['pass', 'sendtest']);
    }

     public function index()
     {
        $this->loadModel('Formations');
        $this->loadModel('Contracts');

        $users = $this->Users->find('all')->where(['role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0e'])->contain(['Formations']);
        //print_r($users->toArray()); die();
        $title = "Gestion des etudiants";
        $role_id = '0646b17f-edae-426d-8235-3bbbb0240d0e';
        $userEntity = $this->Users->newEntity();
        $formations = $this->Formations->find()->combine('id', 'shortname');
        $contracts = $this->Contracts->find()->combine('id', 'name');
        //print_r($formations->toArray()); die();
        $this->set(compact(['users','title', 'userEntity','formations', 'contracts','role_id']) );
    }

    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function pass(){
        $hasher = new DefaultPasswordHasher();
        echo $hasher->hash('123456'); die();
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($usr = $this->Users->save($user)) {
                if (!empty($this->request->getData()['universal_year'])) {
                    $this->loadModel('Years');
                    $data = ['syear' => $this->request->getData()['universal_year'], 'user_id'=>$usr->id];
                    $year = $this->Years->newEntity();
                    $this->Years->patchEntity($year, $data);
                    //$this->Years->updateAll(['current_year'=>'0'],['user_id'=>])
                    if (!$this->Years->save($year)) {
                        print_r($year); die();
                    }
                }
                
                $this->sendCredentials($this->request->getData());
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(Router::url( $this->referer(), true ));
            }
            print_r($user); die();
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function edit()
    {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->get($this->request->getData()['id'], [
                'contain' => []
            ]);
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(Router::url( $this->referer(), true ));
            } else {
                print_r($user); die();
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function delete($id = null)
    {
        //$this->referer(); die();
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                if ($user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0a' || $user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0b' || $user['role_id'] === '0646b17f-edae-426d-8235-3bbbb0240d0f') {
                    return $this->redirect(['prefix'=>'admin', 'controller'=>'Users','action'=>'index']);
                }else{
                    return $this->redirect('/');
                }
            }else{
                $msg = 'Utilisateur ou Mot de passe Incorrecte';
            }
            $this->set(compact('msg'));
            //$this->Flash->error(__(''));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function profile()
    {
       $id = $this->request->session()->read('Auth.User')['id'];
       $user = $this->Users->get($id); 
       $this->loadModel('Countries');
       $countries = $this->Countries->find('all');
       $this->set(compact('user','countries'));
    }

    public function changepassword(){
        $id = $this->request->session()->read('Auth.User')['id'];
        $user = $this->Users->get($id); 
        $passwordhasher = new DefaultPasswordHasher;
        if (!$passwordhasher->check($this->request->getData()['password'], $user->password)) {
            echo "oldpassnomatch";
        }else if (strlen($this->request->getData()['newpassword']) < 6) {
            echo "lengtherror";
        }else if ($this->request->getData()['newpassword'] != $this->request->getData()['renewpassword']) {
            echo "newpassnomatch";
        }else{
            $user = $this->Users->patchEntity($user,['password'=> $this->request->getData()['newpassword']]);
                if ($this->Users->save($user)) {

                    echo "success";
                }else{
                    echo "fail";
                }
        }
        $this->autoRender=false;
    }

    public function sendCredentials($data){
        $fullname = ucfirst($data['firstname']).'. '.ucfirst($data['lastname']);

        $message = "Bonjour ".$fullname.", </br></br> Votre compte est activé, vous pouvez vous connecter sur <a href=''>la plateforme</a> en utilisant les données d'authentification suivantes : </br><ul><li>Nom d'utilisateur : <b>".$data['email']."</b></li><li>Mot de passe: <b>".$data['password']."</b></li></ul>";
        $email = new Email();
        $email->setFrom(['admin@psa.com' => 'PSA'])
            ->setEmailFormat('html')
            ->setTo($data['email'])
            ->setSubject('PSA: '.__('Vos données d\'authentification'));
        if ($email->send($message)) {
            return true;
        }else{
            return false;
        }
    }

    public function sendtest(){
        $this->sendCredentials(['firstname'=>'yassine', 'lastname'=>'el khanboubi', 'email'=>'yassine.elkhanboubi@gmail.com', 'password'=>'123456789']);
    }

    /*public function tutor(){
        $this->loadModel('Roles');
        $users = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Tuteur%']));
                             })
                            ->contain(['Roles']);

        $title = "Gestion des Tuteur";
        $userEntity = $this->Users->newEntity();
        $roles = $this->Roles->find()->where(['name LIKE '=> '%Tuteur%'])->combine('id', 'name');
        $this->set(compact('users', 'title', 'userEntity', 'roles'));
    }*/
    public function univtutor(){
        $this->loadModel('Roles');
        $users = $this->Users->find()
                            ->where(function($exp){
                                return $exp->or_([
                                    'Users.role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0d',
                                    'Users.has_both_roles'=>'1'
                                ]);
                            });
        //print_r($users->toArray()); die();
        $title = "Gestion des Tuteurs Universitaires";
        $userEntity = $this->Users->newEntity();
        $this->set(compact('users', 'title', 'userEntity'));
    }

    public function companytutor(){
        $this->loadModel('Roles');
        $users = $this->Users->find('all')
                            ->where(function($exp){
                            return $exp->or_([
                                    'Users.role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0c',
                                    'Users.has_both_roles'=>'1'
                                ]);
                            });
        //print_r($users->toArray()); die();
        $title = "Gestion des Tuteurs d'Entreprises";
        $userEntity = $this->Users->newEntity();
        $this->set(compact('users', 'title', 'userEntity'));
    }

    public function managetutors($id = null){
        $this->loadModel('Roles');
        $id = (empty($id))? $this->request->getData()['id'] : $id;
        //$user = $this->Users->get($this->request->getData()['id'],['contain'=>['Tutors']);
        $univtutors = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Tuteur Pédagogique%']));
                             })->order(['lastname'=>'ASC']);
        //print_r($univtutors->toArray()); die();
        $exttutors = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Tuteur Entreprise%']));
                             })->order(['lastname'=>'ASC']);
        //print_r($exttutors->toArray()); die();
        $user = $this->Users->find()->where(['id'=>$id])
                                            ->contain(['Tutors'/*=>['Roles']*/])->first();
        //print_r($user); die();
                                         
        $title = "Gestion des association Etudiant -> Tuteurs";
        $this->set(compact('user', 'title', 'univtutors','exttutors'));
    }

    public function tutoraffect(){
        $this->loadModel('Roles');
        $etudiants = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Etudiant%']));
                             })
                            ->contain(['Roles']);
        $tuteurspedagogique = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Tuteur Pédagogique%']));
                             })
                            ->contain(['Roles']);
        $tuteursentreprise = $this->Users->find()
                            ->where(function($exp){
                                return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%Tuteur Entreprise%']));
                             })
                            ->contain(['Roles']);
        $this->set(compact('etudiants','tuteurspedagogique','tuteursentreprise'));
    }

    public function deletestudenttutor($id = null){
        
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('StudentsTutors');
        $userassoc = $this->StudentsTutors->get($id);
        $user_id = $userassoc->student_id;

        if (!$this->StudentsTutors->delete($userassoc)) {
            echo "Error"; die();
        }

        return $this->redirect(['controller' => 'Users', 'action' => 'managetutors', $user_id]);
    }

    public function year()
    {
        $users = $this->Users->find('all')->where(['role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0e'])
                                          ->contain(['Formations','Years'=>[
                                                                    'sort'=>['syear'=>'DESC']
                                                                ]
                                                    ]);
        $title = "Gestion des années Universitaires";
        $this->set(compact('users', 'title'));
    }

    public function nextyear($id = null)
    {
        $this->loadModel('Years');
        if ($this->request->is('post') && !empty($this->request->getData()['ids'])) {
            //IF A SET OF STUDENTS
            foreach ($this->request->getData()['ids'] as $id) {
                $current_year = $this->Years->find()->where(['user_id'=>$id, 'current_year'=>'1'])->first();
                //update all rows as NOT current Year to keep history
                $this->Years->updateAll(['current_year'=>'0'],['user_id'=>$id]);
                $nextyear = ['user_id'=>$id,'syear'=>$current_year->syear + 1];
                $newyear = $this->Years->newEntity();
                $this->Years->patchEntity($newyear, $nextyear);
                if (!$this->Years->save($newyear)) {
                    print_r($nextyear); die();
                }
            }
            echo "success";
            exit;
        }
        //IF ONLY ONE STUDENT
        $this->loadModel('Years');
        $current_year = $this->Years->find()->where(['user_id'=>$id, 'current_year'=>'1'])->first();
        //update all rows as NOT current Year to keep history
        $this->Years->updateAll(['current_year'=>'0'],['user_id'=>$id]);
        $nextyear = ['user_id'=>$id,'syear'=>$current_year->syear + 1];
        $newyear = $this->Years->newEntity();
        $this->Years->patchEntity($newyear, $nextyear);
        if (!$this->Years->save($newyear)) {
            print_r($nextyear); die();
        }
        return $this->redirect(Router::url( $this->referer(), true ));
    }

    public function addusertutor()
    {
        if ($this->request->is('post')) {
            $this->loadModel('StudentsTutors');
            $assoc = $this->StudentsTutors->newEntity();
            $data = $this->request->getData();
            $data['current_tutor'] = '0';
            $this->StudentsTutors->patchEntity($assoc, $data);

            if (!$this->StudentsTutors->save($assoc)) {
                echo "Error";die();
            }

            return $this->redirect(['controller' => 'Users', 'action' => 'managetutors', $this->request->getData()['student_id']]);
        }
    }

    public function rdv(){
        $this->loadModel('Rdvs');
        $title = "Admin - Liste des RDVs";
        $rdvs = $this->Rdvs->find()->contain(['Students','IntTutors','ExtTutors','Reports'])->order(['rdv_time'=>'DESC']);
        $this->set(compact('rdvs','title'));
    }

    public function uploadreport()
    {
        $this->loadModel('Reports');
        $tutors = $this->Users->find()->where(['role_id'=>'0646b17f-edae-426d-8235-3bbbb0240d0d'])->contain(['Students']);
        $title = "Gestion des Rapport";
        $reports = $this->Reports->find()->contain(['ExtTutors','IntTutors','Students', 'Rdvs'])->order(['upload_time'=>'DESC']);
        $this->set(compact('tutors','title', 'reports'));
    }

    public function formations()
    {
        $this->loadModel('Formations');
        $this->loadModel('Poles');
        $title = "Gestion des Formations";
        
        if ($this->Auth->user()['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0a') {
            $poles = $this->Poles->find()->where(['admin_id'=>$this->Auth->user()['id']])->contain(['Formations','Users']);
        }else{
            $poles = $this->Poles->find()->contain(['Formations','Users']);
        }
        //print_r($poles->toArray());die();
        //$formations = $this->Formations->find()->order(['fullname'=>'ASC']);

        $this->set(compact('poles','title'));
    }

    public function deleteformation($id = null)
    {
        //$this->referer(); die();
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Formations');
        $user = $this->Formations->get($id);
        if ($this->Formations->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }

    public function addformation()
    {
        $this->loadModel('Formations');
        if ($this->request->is('post')) {
            $f = $this->Formations->newEntity();
            $f = $this->Formations->patchEntity($f, $this->request->getData());

            if ($this->Formations->save($f)) {
                return $this->redirect($this->referer());
            }
            print_r($f); die();
        }
        
    }

    public function editformation()
    {
        $this->loadModel('Formations');
        if ($this->request->is('post')) {
            $f = $this->Formations->get($this->request->getData()['id']);
            $f = $this->Formations->patchEntity($f, $this->request->getData());

            if ($this->Formations->save($f)) {
                return $this->redirect($this->referer());
            }
            print_r($f); die();
        }
        
    }

    public function admins()
    {
        $title = "Gestion des Administrateurs";
        $this->loadModel('Roles');
        $roles = $this->Roles->find()->select(['id'])->where(['name LIKE'=>'%admin%'])->toArray();
        
        $admins = $this->Users->find()->where(function($exp){
            return $exp->in('Users.role_id', $this->Roles->find()->select(['id'])->where(['name LIKE '=> '%admin%']));
        });
        $this->set(compact('admins','title'));
    }

}
