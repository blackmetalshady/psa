<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StudentsTutors Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\StudentsTutor get($primaryKey, $options = [])
 * @method \App\Model\Entity\StudentsTutor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StudentsTutor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StudentsTutor|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StudentsTutor|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StudentsTutor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StudentsTutor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StudentsTutor findOrCreate($search, callable $callback = null, $options = [])
 */
class StudentsTutorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('students_tutors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'tutor_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date_affectation')
            ->requirePresence('date_affectation', 'create')
            ->notEmpty('date_affectation');

        $validator
            ->scalar('mission')
            ->requirePresence('mission', 'create')
            ->notEmpty('mission');

        $validator
            ->requirePresence('current_tutor', 'create')
            ->notEmpty('current_tutor');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Users'));
        $rules->add($rules->existsIn(['tutor_id'], 'Users'));

        return $rules;
    }
}
