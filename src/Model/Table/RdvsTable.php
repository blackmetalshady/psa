<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rdvs Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RdvsTable|\Cake\ORM\Association\BelongsTo $Rdvs
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Rdv get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rdv newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rdv[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rdv|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rdv|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rdv patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rdv[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rdv findOrCreate($search, callable $callback = null, $options = [])
 */
class RdvsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rdvs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ExtTutors', [
            'className' => 'Users',
            'foreignKey' => 'extutor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('IntTutors', [
            'className' => 'Users',
            'foreignKey' => 'inttutor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Students', [
            'className' => 'Users',
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Reports', [
            'foreignKey' => 'report_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('rdv_date')
            ->requirePresence('rdv_date', 'create')
            ->notEmpty('rdv_date');

        $validator
            ->time('rdv_time')
            ->requirePresence('rdv_time', 'create')
            ->notEmpty('rdv_time');

        $validator
            ->scalar('rdv_location')
            ->maxLength('rdv_location', 255)
            ->requirePresence('rdv_location', 'create')
            ->notEmpty('rdv_location');

        $validator
            ->integer('student_accept')
            ->allowEmpty('student_accept');

        $validator
            ->integer('tutor_accept')
            ->allowEmpty('tutor_accept');

        $validator
            ->integer('cancelled')
            ->allowEmpty('cancelled');
            
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['extutor_id'], 'ExtTutors'));
        $rules->add($rules->existsIn(['inttutor_id'], 'IntTutors'));
        $rules->add($rules->existsIn(['student_id'], 'Students'));
        $rules->add($rules->existsIn(['report_id'], 'Reports'));

        return $rules;
    }
}
