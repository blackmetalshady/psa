<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PolesFormations Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Poles
 * @property |\Cake\ORM\Association\BelongsTo $Formations
 *
 * @method \App\Model\Entity\PolesFormation get($primaryKey, $options = [])
 * @method \App\Model\Entity\PolesFormation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PolesFormation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PolesFormation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PolesFormation|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PolesFormation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PolesFormation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PolesFormation findOrCreate($search, callable $callback = null, $options = [])
 */
class PolesFormationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setTable('poles_formations');

        $this->belongsTo('Poles', [
            'foreignKey' => 'pole_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Formations', [
            'foreignKey' => 'formation_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pole_id'], 'Poles'));
        $rules->add($rules->existsIn(['formation_id'], 'Formations'));

        return $rules;
    }
}
