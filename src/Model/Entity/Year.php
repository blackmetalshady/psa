<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Year Entity
 *
 * @property string $id
 * @property int $syear
 * @property string $user_id
 * @property int $current_year
 *
 * @property \App\Model\Entity\User $user
 */
class Year extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'syear' => true,
        'user_id' => true,
        'current_year' => true,
        'user' => true
    ];
}
