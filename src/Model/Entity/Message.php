<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property string $id
 * @property string $user_id
 * @property string $to_user
 * @property \Cake\I18n\FrozenTime $sendtime
 * @property string $message
 * @property string|null $filename
 * @property string $conversation_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Conversation $conversation
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'to_user' => true,
        'sendtime' => true,
        'message' => true,
        'filename' => true,
        'conversation_id' => true,
        'user' => true,
        'conversation' => true
    ];
}
