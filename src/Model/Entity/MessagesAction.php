<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MessagesAction Entity
 *
 * @property string $id
 * @property string $user_id
 * @property string|null $messages_id
 * @property string|null $conversation_id
 * @property int $is_read
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $action_time
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Message $message
 * @property \App\Model\Entity\Conversation $conversation
 */
class MessagesAction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'messages_id' => true,
        'conversation_id' => true,
        'is_read' => true,
        'is_deleted' => true,
        'action_time' => true,
        'user' => true,
        'message' => true,
        'conversation' => true
    ];
}
