<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Conversation Entity
 *
 * @property string $id
 * @property string $subject
 * @property \Cake\I18n\FrozenTime $conv_time
 * @property string|null $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\MessagesAction[] $messages_actions
 */
class Conversation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'subject' => true,
        'conv_time' => true,
        'user_id' => true,
        'user' => true,
        'messages' => true,
        'messages_actions' => true
    ];
}
