<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StudentsTutor Entity
 *
 * @property string $id
 * @property string $student_id
 * @property string $tutor_id
 * @property \Cake\I18n\FrozenDate $date_affectation
 * @property string $mission
 * @property int $current_tutor
 *
 * @property \App\Model\Entity\User $user
 */
class StudentsTutor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'student_id' => true,
        'tutor_id' => true,
        'date_affectation' => true,
        'mission' => true,
        'current_tutor' => true,
        'user' => true
    ];
}
