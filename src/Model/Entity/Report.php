<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Report Entity
 *
 * @property string $id
 * @property string $tutor_id
 * @property string $student_id
 * @property \Cake\I18n\FrozenTime $upload_time
 * @property string $filename
 *
 * @property \App\Model\Entity\User $user
 */
class Report extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'inttutor_id' => true,
        'exttutor_id' => true,
        'student_id' => true,
        'upload_time' => true,
        'filename' => true,
        'user' => true,
        'reportmessage'=>true
    ];
}
