<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PolesFormation Entity
 *
 * @property string $id
 * @property string $pole_id
 * @property string $formation_id
 */
class PolesFormation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'pole_id' => true,
        'formation_id' => true
    ];
}
