<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rdv Entity
 *
 * @property string $id
 * @property string $extutor_id
 * @property string $inttutor_id
 * @property string $student_id
 * @property \Cake\I18n\FrozenDate $rdv_date
 * @property \Cake\I18n\FrozenTime $rdv_time
 * @property string $rdv_location
 * @property bool $student_accept
 * @property bool $tutor_accept
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Rdv $rdv
 */
class Rdv extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'extutor_id' => true,
        'inttutor_id' => true,
        'student_id' => true,
        'rdv_date' => true,
        'rdv_time' => true,
        'rdv_location' => true,
        'student_accept' => true,
        'tutor_accept' => true,
        'user' => true,
        'rdv' => true,
        'cancelled'=>true,
        'report_id'=>true
    ];
}
