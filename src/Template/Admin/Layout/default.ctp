<!DOCTYPE html>
<html lang="en">

<head>

    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $this->fetch('title') ?></title>
    <?= $this->Html->css(['bootstrap.min.css','bootstrap-select.min.css','metisMenu.min.css','sb-admin-2.css','font-awesome.min.css','dataTables.bootstrap.css','dataTables.responsive.css']) ?>
    <?= $this->Html->script(['jquery.min.js','bootstrap.min.js','bootstrap-select.min.js','metisMenu.min.js','sb-admin-2.js','jquery.dataTables.min.js','dataTables.bootstrap.min.js','jquery.flot.js','jquery.flot.resize.js','excanvas.min.js','jquery.flot.tooltip.min.js','jquery.flot.time.js','jquery.flot.axislabels','jscolor.min.js','tinymce/tinymce.min.js','jquery-ui.js','jquery.form.min.js']) ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?= $this->Html->image('logo.jpg', [
                    'alt' => 'Total',
                    'style'=>'max-width: 220px;margin-left: 20px;',
                    'url' => ['controller' => 'Pages', 'action' => 'display', 'home']
                ]);?>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <?php if (!empty($notifications)) : ?>
                        <div class="notifcircle"><?= sizeof($notifications)?></div>
                    <?php endif; ?>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw" style="font-size: 22px;"></i>  <i class="fa fa-caret-down" style="font-size: 22px;"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <?php if (empty($notifications)) :?>
                        <li>
                            <a href="#">
                                <div>
                                    <center>Aucune nouvelle notification</center>
                                </div>
                            </a>
                        </li>
                        <?php else: ?>
                            <?php 
                                $numItems = count($notifications);
                                $i = 0;
                            ?>
                            <?php foreach ($notifications as $notif) :?>
                                <li>
                                    <a href="<?= $notif['url'];?>">
                                        <div>
                                            <i class="fa fa-<?= $notif['icon'] ?> fa-fw"></i> 
                                            <span style="color: red;">
                                                <?= $notif['value'];?>
                                            </span>
                                            <?= $notif['message'];?>
                                        </div>
                                    </a>
                                </li>
                                <?php 
                                  if(++$i !== $numItems) {
                                    echo '<li class="divider"></li>';
                                  }
                                 ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw" style="font-size: 22px;"></i>  
                        <i class="fa fa-caret-down" style="font-size: 22px;"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><?= $this->Html->link('<i class="fa fa-sign-out fa-fw"></i>Se Deconnecter',[
                            'controller'=>'Users',
                            'action'=>'logout'
                        ],['escape'=>false]);?>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0f' || $loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0b'): ?>
                        <li>
                            <a href="#"><i class="fa fa-graduation-cap fa-fw" style="font-size: 22px;"></i> Gestion des Etudiants<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Ajout/Suppression/Modification',['controller' => 'Users', 'action' => 'index']); ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Année Universitaire',['controller' => 'Users', 'action' => 'year']); ?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php endif ?>
                        <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0b'): ?>
                        <li>
                            <a href="#"><i class="fa fa-graduation-cap fa-fw" style="font-size: 22px;"></i> Gestion des Poles<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Ajout/Suppression/Modification',['controller' => 'Poles', 'action' => 'index']); ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Affectation Admin',['controller' => 'Poles', 'action' => 'adminaffect']); ?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php endif ?>
                        <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0a' || $loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0b'): ?>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw" style="font-size: 22px;"></i> Gest. Admins/Formations<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Ajout Admin',['controller' => 'Users', 'action' => 'admins']); ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Gestion des Formations',['controller' => 'Users', 'action' => 'formations']); ?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php endif ?>
                        <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0f' || $loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0b'): ?>
                        <li>
                            <a href="#"><i class="fa fa-map fa-fw" style="font-size: 22px;"></i> Gestion des Tuteurs<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Tuteurs Universitaire',['controller' => 'Users', 'action' => 'univtutor']); ?>
                                </li>
                                <li>
                                    <?= $this->Html->link('Tuteurs d\'entreprise',['controller' => 'Users', 'action' => 'companytutor']); ?>
                                </li>
                                <!--<li>
                                    <?= $this->Html->link('Affectation Etudiants/Tuteur',['controller' => 'Users', 'action' => 'tutoraffect']); ?>
                                </li>-->
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart fa-fw" style="font-size: 22px;"></i> Gestion des Rapports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('Dépos de rapport',['controller' => 'Users', 'action' => 'uploadreport'])?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-home fa-fw" style="font-size: 22px;"></i>
                                Gestion des Rendez-Vous
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?= $this->Html->link('List des RDV',['controller' => 'Users', 'action' => 'rdv'])?>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <?= $this->fetch('content') ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

</html>
