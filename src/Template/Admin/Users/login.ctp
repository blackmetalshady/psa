<!-- File: src/Template/Users/login.ctp -->
<?= $this->layout = false; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>
<?= $this->Html->css(['bootstrap.min.css','metisMenu.min.css','sb-admin-2.css','font-awesome.min.css']) ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Admin Authentification</h3>
                    </div>
                    <div class="panel-body">
                        <?php if (isset($msg)) :?>
                            <div class="alert alert-danger">
                                <?= $msg; ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->Form->create() ?>
                            <fieldset>
                                <div class="form-group">
                                    <?= $this->Form->control('email',[
                                    'label' => 'Nom d\'Utilisateur',
                                    'class'=>'form-control',
                                    'placeholder'=>'Nom d\'Utilisateur',
                                    'autofocus'=>'autofocus'
                                    ]) ?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->control('password',[
                                    'label' => 'Mot de Passe',
                                    'class'=>'form-control',
                                    'placeholder'=>'Mot de Passe']
                                    ) ?>
                                </div>
                                <?= $this->Form->submit(__('Login'),['class'=>'btn btn-lg btn-primary btn-block']); ?>
                            </fieldset>
                        <?= $this->Form->end() ?>
                    </div>
                    <?php //echo (isset($msg) ? $msg : "")  ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->Html->script(['jquery.min.js','bootstrap.min.js','metisMenu.min.js','raphael-min.js','morris.min.js','morris-data.js','sb-admin-2.js']) ?>

</body>

</html>