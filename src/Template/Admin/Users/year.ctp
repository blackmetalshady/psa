<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Années d'Etude</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Etudiants
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <input type="button" class="btn btn-lg btn-primary" id="passall" value="Passez à l'Année Suivante" style="margin-bottom: 20px; float: right;" disabled/>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="checkall"></th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>email</th>
                                <th>Formation</th>
                                <th>Année</th>
                                <th>Historique</th>
                                <th>Pass. Ann. Suivante</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($users as $user) :?>
                            <tr class="">
                                <td><input type="checkbox" name="pass[]" class="tonextyear" value="<?= $user->id; ?>"></td>
                                <td><?= $user->firstname; ?></td>
                                <td><?= $user->lastname; ?></td>
                                <td><?= $user->email; ?></td>
                                <td><?= $user->formation->shortname; ?></td>
                                <td class="center">
                                    <?php foreach ($user->years as $year): ?>
                                        <?php if ($year->current_year == '1'){
                                            echo $year->syear;
                                            break;
                                        }?>
                                    <?php endforeach ?>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#history<?= $user->id ?>">Voir</a>
                                    <!-- Modal Add Car-->
                                    <div class="modal fade" id="history<?= $user->id ?>" tabindex="-1" role="dialog" aria-labelledby="historyLabel<?= $user->id ?>" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="historyLabel<?= $user->id ?>">
                                                        <?= $user->firstname.' '.$user->lastname;; ?>
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Année</th>
                                                                    <th>Actuel</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($user->years as $year): ?>
                                                                    <tr>
                                                                        <td><?= $year->syear; ?></td>
                                                                        <td><?= ($year->current_year == '1')? '<i class="fa fa-check" aria-hidden="true"></i>': '' ?></td>
                                                                    </tr>
                                                                <?php endforeach ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <?= $this->Form->postLink(
                                                        'Passer à l\'année Suivante?',
                                                        ['controller' => 'Users', 'action' => 'nextyear', $user->id],
                                                        ['confirm' => 'Etes vous sur? (CETTE ACTION EST IRREVERSIBLE)','class'=>'btn btn-success btn-default','escape'=>false]
                                                    ); ?>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </div>
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-chevron-right"></i>',
                                        ['controller' => 'Users', 'action' => 'nextyear', $user->id],
                                        ['confirm' => 'Etes vous sur? (CETTE ACTION EST IRREVERSIBLE)','class'=>'btn btn-success btn-circle','escape'=>false]
                                    ); ?>
                                    <!-- Modal Edit Etudiant -->
                                    <div class="modal fade" id="editCar<?= $user->id ?>" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editCarLabel">
                                                        Edition des informations des Etudiants
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $user->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $user->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create($userEntity,['name'=>'EditUserForm','url' => ['action' => 'edit']]) ?>
                                                    <?= $this->Form->hidden('id', ['value'=>$user->id]);?>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('firstname',[
                                                            'label' => 'Prénom',
                                                            'value'=>$user->firstname,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Prénom'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('lastname',[
                                                            'label' => 'Nom',
                                                            'value'=>$user->lastname,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Nom'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('email',[
                                                            'label' => 'E-mail',
                                                            'value'=>$user->email,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'E-mail'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->textarea('address',[
                                                            'label' => 'Adresse',
                                                            'value'=>$user->address,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Adresse'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->textarea('company_address',[
                                                            'label' => 'Adresse de l\'entreprise',
                                                            'value'=>$user->company_address,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Adresse de l\'entreprise'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('phone',[
                                                            'label' => 'Téléphone',
                                                            'value'=>$user->phone,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Téléphone'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div class="input text required">
                                                            <?= $this->Form->label('Formation'); ?>
                                                            <?=$this->Form->select(
                                                                'formation_id',
                                                                $formations,
                                                                ['empty' => '(choisissez)', 'class'=>'form-control', 'required'=>'true', 'default'=>$user->formation_id]
                                                            ); ?>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div class="input text required">
                                                            <?= $this->Form->label('Annee Universitaire'); ?>
                                                            <?=$this->Form->select(
                                                                'universal_year',
                                                                ['2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019'],
                                                                ['empty' => '(choisissez)', 'class'=>'form-control', 'required'=>'true', 'default'=>$user->universal_year]
                                                            ); ?>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <?= $this->Form->hidden('role_id', ['value'=>$role_id]);?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>