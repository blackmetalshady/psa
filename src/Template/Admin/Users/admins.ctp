<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Administrateurs</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Administrateurs
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addCar" style="float: right; margin-bottom: 25px;">
                    Ajouter un Administrateur
                </button>
                <br/>
                <!-- Modal Add Car-->
                <div class="modal fade" id="addCar" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addCarLabel">
                                    Ajouter un Nouveau Administrateur
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create(null,['name'=>'adminsForm','url' => ['action' => 'add']]) ?>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('firstname',[
                                        'label' => 'Prénom',
                                        'class'=>'form-control',
                                        'placeholder'=>'Prénom',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('lastname',[
                                        'label' => 'Nom',
                                        'class'=>'form-control',
                                        'placeholder'=>'Nom',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('email',[
                                        'label' => 'E-mail',
                                        'class'=>'form-control',
                                        'placeholder'=>'E-mail',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group input-group" style="width: 100%">
                                        <?= $this->Form->control('password',[
                                        'label' => 'Mot de Passe',
                                        'class'=>'form-control',
                                        'id'=>'pass',
                                        'type'=>'password',
                                        'style'=>"border-top-right-radius: 0;border-bottom-right-radius: 0;",
                                        'placeholder'=>'Mot de Passe',
                                        'required'=>true
                                        ]
                                    ) ?>
                                    <button class="btn btn-default pass-gen" type="button" style="display: inline-block;border-top-left-radius: 0;border-bottom-left-radius: 0;"><i class="fa fa-refresh"></i></button>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->textarea('address',[
                                        'label' => 'Adresse',
                                        'class'=>'form-control',
                                        'placeholder'=>'Adresse',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('phone',[
                                        'label' => 'Téléphone',
                                        'class'=>'form-control',
                                        'placeholder'=>'Téléphone',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <div class="input text required">
                                        <?= $this->Form->label('Role'); ?>
                                        <?php if ($loggedIn['role_id'] == "0646b17f-edae-426d-8235-3bbbb0240d0b"): ?>
                                            <?=$this->Form->select(
                                                'role_id',
                                                ['0646b17f-edae-426d-8235-3bbbb0240d0b'=>'Admin de l’Université','0646b17f-edae-426d-8235-3bbbb0240d0a'=>'Admin Pôle','0646b17f-edae-426d-8235-3bbbb0240d0f'=>'Admin de Formation'],
                                                ['class'=>'form-control', 'required'=>'true', 'default'=>'0646b17f-edae-426d-8235-3bbbb0240d0f']
                                            ); ?>
                                        <?php else: ?>
                                            <?=$this->Form->select(
                                                'role_id',
                                                ['0646b17f-edae-426d-8235-3bbbb0240d0f'=>'Admin de Formation'],
                                                ['class'=>'form-control', 'style'=>'cursor: not-allowed','required'=>'true', 'default'=>'0646b17f-edae-426d-8235-3bbbb0240d0f', 'readonly'=>true]
                                            ); ?>
                                        <?php endif ?>
                                        
                                    </div>
                                </div>
                                <br/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal Add Car -->
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>email</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($admins as $admin) :?>
                                <?php //print_r($user); die(); ?>
                            <tr class="">
                                <td><?= $admin->firstname; ?></td>
                                <td><?= $admin->lastname; ?></td>
                                <td><?= $admin->email; ?></td>
                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editCar<?= $admin->id ?>" onclick="clearform();"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Car -->
                                    <div class="modal fade" id="editCar<?= $admin->id ?>" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editCarLabel">
                                                        Edition des informations des Etudiants
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $admin->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $admin->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create(null,['name'=>'EditUserForm','url' => ['action' => 'edit']]) ?>
                                                    <?= $this->Form->hidden('id', ['value'=>$admin->id]);?>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('firstname',[
                                                            'label' => 'Prénom',
                                                            'value'=>$admin->firstname,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Prénom'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('lastname',[
                                                            'label' => 'Nom',
                                                            'value'=>$admin->lastname,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Nom'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('email',[
                                                            'label' => 'E-mail',
                                                            'value'=>$admin->email,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'E-mail'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->textarea('address',[
                                                            'label' => 'Adresse',
                                                            'value'=>$admin->address,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Adresse'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('phone',[
                                                            'label' => 'Téléphone',
                                                            'value'=>$admin->phone,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Téléphone'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div class="input text required">
                                                            <?= $this->Form->label('Role'); ?>
                                                            <?=$this->Form->select(
                                                                'universal_year',
                                                                ['0646b17f-edae-426d-8235-3bbbb0240d0b'=>'Admin de l’Université','0646b17f-edae-426d-8235-3bbbb0240d0a'=>'Admin Pôle','0646b17f-edae-426d-8235-3bbbb0240d0f'=>'Admin de Formation'],
                                                                ['class'=>'form-control', 'required'=>'true', 'default'=>$admin->role_id]
                                                            ); ?>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Users', 'action' => 'delete', $admin->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer l\'administrateur ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>