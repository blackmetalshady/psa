<?php $this->assign('title', $title); ?>
<?php //print_r($user); die(); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $user->firstname.' '.$user->lastname ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
    	<div class="panel panel-default">
            <div class="panel-heading">
                Tuteurs Pédagogiques
                <i class="fa fa-plus" data-toggle="modal" data-target="#univtutor" style="color: lightgreen; font-size: 20px; float: right; margin-right: 5px;cursor: pointer;"></i>
                <!-- Modal -->
                <div class="modal fade" id="univtutor" tabindex="-1" role="dialog" aria-labelledby="univtutor" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="historyLabel<?= $user->id ?>">
                                    <?= $user->firstname.' '.$user->lastname;; ?>
                                </h3>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->create(null,['name'=>'CarForm','url' => ['action' => 'addusertutor']]) ?>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->label('Tuteurs Pédagogique'); ?>
                                    <select class="form-control" name="tutor_id" required>
                                        <option value=""> Veuillez choisir un Tuteur</option>
                                        <?php foreach ($univtutors as $ut): ?>
                                            <?php $i = 0; ?>
                                            <?php foreach ($user->tutors as $tutor): ?>
                                                <?php if ($ut->id == $tutor->id){
                                                    $i++;
                                                } ?>
                                            <?php endforeach ?>
                                            <?php if ($i == 0 ) {
                                                echo '<option value="'.$ut->id.'">'.$ut->lastname.' '.$ut->firstname.'</option>';
                                            } ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->label('Date d\'affectation'); ?>
                                    <input type="date" class="form-control" name="date_affectation" required>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->textarea('mission',[
                                        'label' => 'Mission',
                                        'class'=>'form-control',
                                        'placeholder'=>'Mission',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <input type="hidden" name="student_id" value="<?= $user->id; ?>" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Affecter</button>
                            </div>
                            <?= $this->Form->end() ?>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
            </div>
            <div class="panel-body">
            	<?php foreach ($user->tutors as $tutor): ?>
                    <?php //print_r($tutor->_joinData->id); die(); ?>
            		<?php if ($tutor->role_id == '0646b17f-edae-426d-8235-3bbbb0240d0d' || $tutor->has_both_roles =='1'): ?>
            			<div>
                            <?= $this->Form->postLink(
                                '<i class="fa fa-times" style="display: inline-block; color: red; margin-right: 1%;"></i>',
                                ['controller' => 'Users', 'action' => 'deletestudenttutor', $tutor->_joinData->id],
                                ['confirm' => 'Etes vous sur de vouloir supprimer l\'association ?','escape'=>false]
                            ); ?>
            				<h4 style="display: inline-block; min-width: 46%;">
            					<?= $tutor->firstname.' '.$tutor->lastname ?>
            				</h4>
            				<div style="display: inline-block; width: 48%;">
            					<input type="checkbox" name="" value="<?= $tutor->_joinData->current_tutor?>" checked="<?= ($tutor->_joinData->current_tutor == '1') ? 'true' : 'false'?>" style="float: right;">
            				</div>
            			</div>
            		<?php endif ?>
            	<?php endforeach ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    	<div class="panel panel-default">
            <div class="panel-heading">
                Tuteurs d'Entreprise
                <i class="fa fa-plus" data-toggle="modal" data-target="#Exttutor" style="color: lightgreen; font-size: 20px; float: right; margin-right: 5px;cursor: pointer;"></i>
                <!-- Modal -->
                <div class="modal fade" id="Exttutor" tabindex="-1" role="dialog" aria-labelledby="Exttutor" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="historyLabel<?= $user->id ?>">
                                    <?= $user->firstname.' '.$user->lastname;; ?>
                                </h3>
                            </div>
                            <div class="modal-body">
                                <?= $this->Form->create(null,['name'=>'ExtTutorForm','url' => ['action' => 'addusertutor']]) ?>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->label('Tuteurs Pédagogique'); ?>
                                    <select class="form-control" name="tutor_id" required>
                                        <option value=""> Veuillez choisir un tuteur</option>
                                        <?php foreach ($exttutors as $et): ?>
                                            <?php foreach ($user->tutors as $tutor): ?>
                                                <?php if ($et->id != $tutor->id): ?>
                                                    <option value="<?= $et->id; ?>"><?= $et->lastname.' '.$et->firstname; ?></option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->label('Date d\'affectation'); ?>
                                    <input type="date" class="form-control" name="date_affectation" required>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->textarea('mission',[
                                        'label' => 'Mission',
                                        'class'=>'form-control',
                                        'placeholder'=>'Mission',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <input type="hidden" name="student_id" value="<?= $user->id; ?>" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Affecter</button>
                            </div>
                            <?= $this->Form->end() ?>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
            </div>
            <div class="panel-body">
            	<?php foreach ($user->tutors as $tutor): ?>
            		<?php if ($tutor->role_id == '0646b17f-edae-426d-8235-3bbbb0240d0c' || $tutor->has_both_roles =='1'): ?>
            			<div>
                            <?= $this->Form->postLink(
                                '<i class="fa fa-times" style="display: inline-block; color: red; margin-right: 1%;"></i>',
                                ['controller' => 'Users', 'action' => 'deletestudenttutor', $tutor->_joinData->id],
                                ['confirm' => 'Etes vous sur de vouloir supprimer l\'association ?','escape'=>false]
                            ); ?>
            				<h4 style="display: inline-block; min-width: 46%;">
            					<?= $tutor->firstname.' '.$tutor->lastname ?>
            				</h4>
            				<div style="display: inline-block;width: 48%;">
            					<input type="checkbox" name="" value="<?= $tutor->_joinData->current_tutor?>" checked="<?= ($tutor->_joinData->current_tutor == '1') ? 'true' : 'false'?>" style="float: right;">
            				</div>
            			</div>
            		<?php endif ?>
            	<?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>