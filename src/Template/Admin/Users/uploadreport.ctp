<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Etudiants</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Etudiants
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addCar" style="float: right; margin-bottom: 25px;">
                    Ajouter un Rapport
                </button>
                <br/>
                <!-- Modal Add Car-->
                <div class="modal fade" id="addCar" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addCarLabel">
                                    Ajouter un Rapport
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create(null, ['url'=>['controller'=>'Reports', 'action'=>'add'], 'type' => 'file', 'class'=>'report-form']) ?>

                                <div class="form-group" style="display: block;">
                                    <label for="inttutorID" style="min-width: 180px;">Tuteur Pédagogique</label>
                                    <select name="inttutor_id" class="form-control" id="inttutorID" style="display: inline-block;" required>
                                        <option value="">--</option>
                                        <?php foreach ($tutors as $tutor): ?>
                                            <option value="<?= $tutor->id; ?>" ><?= $tutor->firstname.' '.$tutor->lastname; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <label for="studentID" style="min-width: 180px;">Etudiant </label>
                                    <select name="student_id" class="form-control studentslist" style="display: inline-block;" required>
                                        <option value="">--</option>
                                        <?php foreach ($tutors as $tutor): ?>
                                            <?php foreach ($tutor->students as $student): ?>
                                                <option value="<?= $student->id; ?>" class="student std_<?= $tutor->id; ?>" style="display: none;"><?= $student->firstname.' '.$student->lastname; ?></option>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <label for="exttutorID" style="min-width: 180px;">Tuteur d'Entreprise</label>
                                    <select name="exttutor_id" class="form-control" id="exttutorID" style="display: inline-block;" required>
                                        <option value="">--</option>
                                    </select>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->textarea('reportmessage',[
                                        'label' => 'Texte',
                                        'class'=>'form-control',
                                        'placeholder'=>'Texte',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <label for="studentID" style="min-width: 180px;">Liste des RDVs</label>
                                    <select name="rdv_id" class="form-control" id="rdvs" style="display: inline-block;" required>
                                        <option value="">--</option>
                                    </select>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <label for="reportfile" style="min-width: 180px;">Rapport </label>
                                    <?= $this->Form->file('rapport',['id'=>'reportfile', 'required'=>true,'style'=>'display: inline-block']); ?>
                                </div>
                                <br/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal Add Car -->
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                        <thead>
                            <tr>
                                <th>Tuteur Pédagogique</th>
                                <th>Tuteur d'Entreprise</th>
                                <th>Etudiant</th>
                                <th>texte</th>
                                <th>Rapport</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($reports as $report) :?>
                                <?php //print_r($report); die(); ?>
                            <tr class="">
                                <td><?= $report->int_tutor->firstname.' '.$report->int_tutor->lastname; ?></td>
                                <td><?= $report->ext_tutor->firstname.' '.$report->ext_tutor->lastname; ?></td>
                                <td><?= $report->student->firstname.' '.$report->student->lastname; ?></td>
                                <td><?= $report->reportmessage; ?></td>
                                <td><a href="/files/<?= $report->filename; ?>"><?= $report->filename; ?></a></td>
                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#edit<?= $report->id ?>" onclick="clearform();"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Car -->
                                    <div class="modal fade" id="edit<?= $report->id ?>" tabindex="-1" role="dialog" aria-labelledby="editLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editLabel">
                                                        Edition des informations du rapport
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $report->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $report->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    
                                                    <?= $this->Form->create(null, ['url'=>['controller'=>'Reports', 'action'=>'edit'], 'type' => 'file', 'class'=>'report-form']) ?>
                                                    <?= $this->Form->hidden('id', ['value'=>$report->id]);?>
                                                    <div class="form-group" style="display: block;">
                                                        <label for="editintTutorID" style="min-width: 180px;">Tuteur Pédagogique </label>
                                                        <select name="inttutor_id" class="form-control" id="editintTutorID" style="display: inline-block;" required>
                                                            <option value="">--</option>
                                                            <?php foreach ($tutors as $tutor): ?>
                                                                <option value="<?= $tutor->id; ?>" selected="<?= ($tutor->id == $report->int_tutor->id)? 'true':'false'?>"><?= $tutor->firstname.' '.$tutor->lastname; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <label for="studentID" style="min-width: 180px;">Etudiant </label>
                                                        <select name="student_id" class="form-control studentslist" style="display: inline-block;" required>
                                                            <option value="">--</option>
                                                            <?php foreach ($tutors as $tutor): ?>
                                                                <?php if ($tutor->id == $report->int_tutor->id): ?>
                                                                    <?php foreach ($tutor->students as $student): ?>
                                                                        <option value="<?= $student->id; ?>" class="std_<?= $student->id; ?>" selected="<?= ($student->id == $report->student->id)? 'true':'false'?>"><?= $student->firstname.' '.$student->lastname; ?></option>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <label for="editexttutorID" style="min-width: 180px;">Tuteur d'Entreprise</label>
                                                        <select name="exttutor_id" class="form-control" id="editexttutorID" style="display: inline-block;" required>
                                                            <option value="">--</option>
                                                            <option value="<?= $report->ext_tutor->id?>" selected><?= $report->ext_tutor->firstname." ".$report->ext_tutor->lastname; ?></option>
                                                        </select>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->textarea('reportmessage',[
                                                            'label' => 'Texte',
                                                            'class'=>'form-control',
                                                            'value'=>$report->reportmessage,
                                                            'required'=>true,
                                                            'placeholder'=>'Texte'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <label for="studentID" style="min-width: 180px;">Liste des RDVs</label>
                                                        <select name="rdv_id" class="form-control" id="rdvs" style="display: inline-block;">
                                                            <option value="">--</option>
                                                            <option value="<?= $report->rdv->id; ?>" selected><?= $report->rdv->rdv_date->i18nFormat('dd/MM/yyyy')." ".$report->rdv->rdv_time->i18nFormat('HH:mm'); ?></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <label for="reportfile" style="min-width: 180px;">Rapport </label><?= $this->Form->file('rapport',['id'=>'reportfile', 'style'=>'display: inline-block']); ?>
                                                    </div>
                                                    <br/>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Reports', 'action' => 'delete', $report->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer le rapport ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    .report-form div{
        text-align: left;
    }
    
</style>