<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Rendez-vous</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Rendez-vous
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                        <thead>
                            <tr>
                                <th>Etudiants</th>
                                <th>Tuteur Pedagogique</th>
                                <th>Tuteur Entreprise</th>
                                <th>Date</th>
                                <th>Heur</th>
                                <th>Emplacement</th>
                                <th>Rapport</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($rdvs as $rdv) :?>
                                <?php //print_r($rdv); die(); ?>
                            <tr class="">
                                <td><?= $rdv->student->firstname.' '.$rdv->student->lastname ?></td>
                                <td><?= $rdv->int_tutor->firstname.' '.$rdv->int_tutor->lastname ?></td>
                                <td><?= $rdv->ext_tutor->firstname.' '.$rdv->ext_tutor->lastname ?></td>
                                <td><?= $rdv->rdv_date->i18nFormat('dd/MM/yyyy'); ?></td>
                                <td><?= $rdv->rdv_time->i18nFormat('HH:mm'); ?></td>
                                <td><?= $rdv->rdv_location ?></td>
                                <td>
                                    <a href="/files/<?= $rdv->report->filename ?>"><?= $rdv->report->filename ?></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>