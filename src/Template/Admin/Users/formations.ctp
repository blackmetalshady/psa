<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Formations</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Poles/Formations
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addFormation" style="float: right; margin-bottom: 25px;">
                    Ajouter une Formation
                </button>
                <br/>
                <!-- Modal Add Formation-->
                <div class="modal fade" id="addFormation" tabindex="-1" role="dialog" aria-labelledby="addFormationLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addFormationLabel">
                                    Ajouter une Nouvelle Formation
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create(null,['name'=>'EditFormationForm','url' => ['action' => 'addformation']]) ?>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('shortname',[
                                        'label' => 'Sigle',
                                        'class'=>'form-control',
                                        'placeholder'=>'Sigle',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->control('fullname',[
                                        'label' => 'Nom Complet',
                                        'class'=>'form-control',
                                        'placeholder'=>'Nom Complet',
                                        'required'=>true
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal Add Formation -->
                <?php foreach ($poles as $pole): ?>
                    <h2 style="display: inline-block;margin-right: 5px;"><?= $pole->name?></h2>
                    <?php if ($loggedIn['role_id'] == "0646b17f-edae-426d-8235-3bbbb0240d0b" && !empty($pole->user)): ?>
                        <h5 style="display: inline-block;">(<b>Gerer par:</b> <?= $pole->user->firstname.' '.$pole->user->lastname ?>)</h5>
                    <?php endif ?>
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                            <thead>
                                <tr>
                                    <th>Nom Complet</th>
                                    <th>Sigle</th>
                                    <th>Modifier</th>
                                    <th>Supprimer</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($pole->formations as $f) :?>
                                    <?php //print_r($user); die(); ?>
                                <tr class="">
                                    <td style="text-align: left;"><?= $f->fullname; ?></td>
                                    <td><?= $f->shortname; ?></td>
                                    <td class="center">
                                        <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editFormation<?= $f->id ?>"><i class="fa fa-pencil"></i>
                                        </button>
                                        <!-- Modal Edit Car -->
                                        <div class="modal fade" id="editFormation<?= $f->id ?>" tabindex="-1" role="dialog" aria-labelledby="editFormationLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="modal-title" id="editFormationLabel">
                                                            Edition des informations des Formations
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="alert alert-danger alert-dismissable" id="msgErr<?= $f->id ?>" style="display: none;">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                            <div style="font-weight: bold;">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-danger" id="err-msg<?= $f->id ?>" style="display: none;">
                                                            Veuillez remplir les champs vide
                                                        </div>
                                                        <?= $this->Form->create(null,['name'=>'EditFormationForm','url' => ['action' => 'editformation']]) ?>
                                                        <?= $this->Form->hidden('id', ['value'=>$f->id]);?>
                                                        <div class="form-group" style="display: block;">
                                                            <?= $this->Form->control('shortname',[
                                                                'label' => 'Sigle',
                                                                'value'=>$f->shortname,
                                                                'class'=>'form-control',
                                                                'placeholder'=>'Sigle',
                                                                'required'=>true
                                                                ]
                                                            ) ?>
                                                        </div>
                                                        <br/>
                                                        <div class="form-group" style="display: block;">
                                                            <?= $this->Form->control('fullname',[
                                                                'label' => 'Nom Complet',
                                                                'value'=>$f->fullname,
                                                                'class'=>'form-control',
                                                                'placeholder'=>'Nom Complet',
                                                                'required'=>true
                                                                ]
                                                            ) ?>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                        <button type="submit" class="btn btn-primary">Modifier</button>
                                                    </div>
                                                    <?= $this->Form->end() ?>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /Modal Edit Car -->
                                    </td>
                                    <td class="center">
                                        <?= $this->Form->postLink(
                                            '<i class="fa fa-times"></i>',
                                            ['controller' => 'Users', 'action' => 'deleteformation', $f->id],
                                            ['confirm' => 'Etes vous sur de vouloir supprimer l\'etudiant ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                        ); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>