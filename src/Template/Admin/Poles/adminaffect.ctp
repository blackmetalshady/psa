<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Poles</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Poles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <a href="/admin/users/admins" class="btn btn-primary btn-lg" style="float: right; margin-bottom: 25px;">Ajouter un Administrateur</a>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-etudiants">
                        <thead>
                            <tr>
                                <th>Pole</th>
                                <th>Administrateur</th>
                                <th>Modifier</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($poles as $p) :?>
                                <?php //print_r($p); die(); ?>
                            <tr class="">
                                <td style="text-align: left;"><?= $p->name; ?></td>
                                <td><?= $p->user->firstname.' '.$p->user->lastname ?></td>
                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editPole<?= $p->id ?>"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Car -->
                                    <div class="modal fade" id="editPole<?= $p->id ?>" tabindex="-1" role="dialog" aria-labelledby="editPoleLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editPoleLabel">
                                                        Edition des informations des Poles
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $p->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $p->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create(null,['name'=>'EditPoleForm','url' => ['action' => 'edit']]) ?>
                                                    <?= $this->Form->hidden('id', ['value'=>$p->id]);?>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->control('name',[
                                                            'label' => 'Nom du Pole',
                                                            'value'=>$p->name,
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Nom Complet',
                                                            'required'=>true
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                    <div class="input text required">
                                                        <?= $this->Form->label('Admin du Pole'); ?>
                                                        <?=$this->Form->select(
                                                            'admin_id',
                                                            $admins,
                                                            ['empty' => '(choisissez)', 'class'=>'form-control', 'required'=>true, 'default'=>$p->admin_id]
                                                        ); ?>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>