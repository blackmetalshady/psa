	<!--pages-starts-->
	<div class="pages">
		<div class="container">
			<div class="col-md-4 banner-grid">
				<h3><i class="fas fa-user-graduate"></i> Alternant</h3>
			 <div class="banner-grid-sec">
				<form>
					<div class="form_details">
						<h4><?= $user['lastname'].' '.$user['firstname'] ?></h4>
						<i class="fas fa-mail-bulk"></i>: <?= $user['email'] ?></br>
						<i class="fas fa-mobile-alt"></i>: <?= $user['phone'] ?></br>
						<i class="far fa-address-card"></i> : <?= $user['address'] ?></br>
					</div>
				 </form>
			 </div>
		 </div>
		 <div class="col-md-4 banner-grid">
			<h3><i class="fas fa-chalkboard-teacher"></i> Tuteur universitaire</h3>
		 <div class="banner-grid-sec">
			<form>
				<div class="form_details">
						<h4>BARRY Catherine</h4>
						<i class="fas fa-mail-bulk"></i> : catherine.barry@u-picardie.fr</br>
						<i class="fas fa-mobile-alt"></i> : 06 00 00 00</br>
						<i class="far fa-address-card"></i> : NOAS PUBLISHING 42 Rue du Général Sarrail 59100 ROUBAIX</br>
				</div>
			 </form>
		 </div>
	 </div>
	 <div class="col-md-4 banner-grid">
		<h3><i class="fas fa-user-tie"></i> Tuteur entreprise</h3>
	 <div class="banner-grid-sec">
		<form>
			<div class="form_details">
					<h4>SABRE Thomas</h4>
					<i class="fas fa-mail-bulk"></i> : tsabre@viewon.fr</br>
					<i class="fas fa-mobile-alt"></i> : 06 10 96 37 10</br>
					<i class="far fa-address-card"></i> : NOAS PUBLISHING 42 Rue du Général Sarrail 59100 ROUBAIX</br>
			</div>
		 </form>
	 </div>
 </div>
		 <div class="clearfix"></div>
		</div>
		</div>	
	</div>	
	<!----pages-end---->