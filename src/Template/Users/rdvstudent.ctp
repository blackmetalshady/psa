<div class="pages">
    <div class="container">
    	<table class="table">
            <thead>
                <tr>
                    <th scope="col">Tuteur</th>
                    <th scope="col">Date</th>
                    <th scope="col">Heur</th>
                    <th scope="col">Emplacement</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach ($rdvs as $r):?>
	            <tr>
	                <td scope="col">
	                    <?= $r->int_tutor->firstname.' '.$r->int_tutor->lastname?>
	                </td>
	                <td scope="col">
	                    <?= $r->rdv_date->i18nFormat('dd/MM/yyyy'); ?>
	                </td>
	                <td scope="col">
	                    <?= $r->rdv_time->i18nFormat('HH:mm'); ?>
	                </td>
	                <td scope="col">
	                    <?= $r->rdv_location; ?>
	                </td>
	                <?php if ($r->cancelled == '0') :?>
	                <td scope="col">
	                	<?php if ($r->{$key} == '0'): ?>
	                		<?= $this->Form->postLink(
	                            '<i class="fa fa-check"></i>',
	                            ['controller' => 'Users', 'action' => 'rdvactions', $r->id, 'accept'],
	                            ['confirm' => 'Etes vous sur de vouloir accepter le RDV ?','class'=>'btn btn-success btn-circle','escape'=>false]
	                        ); ?>

		                    <?= $this->Form->postLink(
	                            '<i class="fa fa-times"></i>',
	                            ['controller' => 'Users', 'action' => 'rdvactions', $r->id, 'refuse'],
	                            ['confirm' => 'Etes vous sur de vouloir refuser le RDV ?','class'=>'btn btn-danger btn-circle','escape'=>false]
	                        ); ?>
                        <?php else: ?>
                        	<?= ($r->{$key} == '1')? '<span style="color: green;"> Accepté</span>' : '<span style="color: red;"> Refusé </span>'?>
                        <?php endif ?>
	                </td>
	                <?php else: ?>
	                <td colspan="4">
	                    <span style="color: red">RDV annule</span>
	                </td>
	                <?php endif; ?>
	            </tr>
                <?php endforeach; ?>
          </tbody>
        </table>
    </div>
</div>
<style type="text/css">
.btn-circle{
	border-radius: 50%;
}
</style>