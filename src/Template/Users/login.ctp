<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>University a Educational Category Flat Bootstarp Responsive Website Template | Home :: w3layouts</title>
<?= $this->Html->css(['bootstrap.css', 'style.css']); ?>
<?= $this->Html->script(['jquery.min.js', 'bootstrap.js', 'responsiveslides.min.js']) ?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

</head>
<body>
<!-- banner -->
<script>  
    $(function () {
      $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
</script>  
<div class="banner">      
 <div class="header">
         <div class="logo">
             <a href="/">
                <?= $this->Html->image('logo.jpg'); ?>
             </a>
         </div>
         <div class="top-menu">
             <span class="menu"></span>
             <ul class="navig">
                 <li class="active"><a href="index.html">Accueil</a></li>
                 <li><a href="gallery.html">Gallerie</a></li>
                 <li><a href="contact.html">Contact</a></li>
             </ul>
         </div>
          <!-- script-for-menu -->
     <script>
            $("span.menu").click(function(){
                $("ul.navig").slideToggle("slow" , function(){
                });
            });
     </script>
     <!-- script-for-menu -->
         <div class="clearfix"></div>
 </div>
  <div class="slider">
     <div class="caption">
         <div class="container">
              <div class="callbacks_container">
                  <ul class="rslides" id="slider">
                        <li><h3>Université de Picardie </br><?= $this->Html->image('signatur.png') ?></h3></li>
                        <li><h3>Vous souhaite la Bienvenue</h3></li>
                        <li><h3>Dispositif de suivi des alternants</h3></li>
                        
                  </ul> 
                    <div class="clearfix"></div>
              </div>
          </div>
      </div>
  </div>
  <div class="banner-grids">
      <div class="container">
         <div class="col-md-4 banner-grid">
                <h3>Authentification</h3>
             <div class="banner-grid-sec">
              <?= $this->Form->create(null, ['url' =>['controller'=>'users','action'=>'login']]); ?>
                    <div class="form_details">
                      <?= $this->Form->control('email', ['placeholder'=>'E-mail', 'class'=>'form-control','label'=>false, 'required'=>true]); ?>
                      <?= $this->Form->control('password', ['placeholder'=>'Mot de Passe', 'class'=>'form-control','style'=>'margin-top: 8px;','label'=>false, 'required'=>true]); ?>
                       <div class="sub-button" style="margin-top: 8px;">
                        <?= $this->Form->submit('Connexion'); ?>
                       </div>
                       <?= $this->Html->link('Mot de passe oublié ?',['controller'=>'users', 'action'=>'forgotpassword']) ?>
                       <a href="#">Mot de passe oublié ?</a></br>
                        <a href="#">Problème technique ?</a>
                    </div>
                    <?= $this->Form->end(); ?>
             </div>
         </div>
         <div class="clearfix"></div>
      </div>
  </div>
</div>
<?= $this->fetch('content'); ?>
<div class="footer">
     <div class="container">
         <div class="ftr-sec">
             <div class="col-md-4 ftr-grid2">
                 <h3>Pages</h3>
                 <ul>
                      <li><a href="index.html"><span></span>Accueil</a></li>
                     <li><a href="contact.html"><span></span>Contact</a></li>
                     <li><a href="recommandations.html"><span></span>Recommandations</a></li>
                 </ul>
             </div>
             <div class="clearfix"></div>
         </div>
     </div>
</div>
<!---->
<div class="copywrite">
     <div class="container">
         <p>Copyright © 2018 Université de Pivardie Jules Verne</p>
     </div>
</div>
<!---->
</body>
</html>