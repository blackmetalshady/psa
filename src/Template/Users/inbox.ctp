<?php //print_r($messages->toArray()); die(); ?>

	<div style="background-color: #4dc47d; min-height: 53px; padding: 10px;">
		<div class="msg-button" style="background-color: lightgreen;">
			<a href="/users/newmessage">Nouveau Message</a>
		</div>
	</div>
<div class="container">
	<div class="col-md-10">
		<div class="category blog-ctgry">
			<h4>Messages</h4>
			<div class="list-group">
				<?php foreach ($conv as $message): ?>
					<a href="/messages/conversation/<?= $message->id; ?>" class="list-group-item">
						<div style="display: inline-block; margin: 10px; width: 20px;"><input type="checkbox"></div>
						<div style="display: inline-block; margin-left: 5px; min-width: 100px;"><?= ($message->user_id == $userid)? 'moi' : $message->user->firstname.' '.$message->user->lastname ?></div>
						<div style="display: inline-block; margin-left: 5px; min-width: 400px;"><?= $message->subject.' ('.sizeof($message->messages).')'; ?></div>
						<div style="display: inline-block; margin-left: 5px"><?= $message->conv_time->i18nFormat('dd/MM/yyyy HH:mm:ss'); ?></div>
						<div style="display: inline-block; margin-right: 25px; float: right;margin-top: 15px;">
							<i class="fa fa-trash" aria-hidden="true"></i>
							<i class="fa fa-envelope-open" aria-hidden="true"></i>
						</div>
					</a>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>