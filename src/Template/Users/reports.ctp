<?php //print_r($loggedIn); die(); ?>
<?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0d'): ?>
<div style="background-color: #4dc47d; min-height: 53px; padding: 10px;">
    <div class="msg-button" style="background-color: lightgreen;">
        <a href="#" aria-hidden="true" data-toggle="modal" data-target="#addReport">Nouveau Rapport</a>
    </div>
</div>
<div class="modal fade" id="addReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter un Rapport de Visite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <?= $this->Form->create(null, ['url'=>['controller'=>'Reports', 'action'=>'add'], 'type' => 'file', 'class'=>'report-form']) ?>
            <div class="form-group row">
                <label for="student" class="col-sm-2 col-form-label">Etudiant</label>
                <div class="col-sm-10">
                  <select name="student_id" class="form-control" id="student" required>
                    <option value="">--</option>
                      <?php foreach ($students->students as $student): ?>
                          <option value="<?= $student->id; ?>"><?= $student->firstname." ".$student->lastname; ?></option>
                      <?php endforeach ?>
                  </select>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <label for="exttutor" class="col-sm-2 col-form-label">Tuteur d'Entreprise</label>
                <div class="col-sm-10">
                  <select name="exttutor_id" class="form-control" id="exttutor_id" required>
                    <option value="">--</option>
                  </select>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <label for="reporttxt" class="col-sm-2 col-form-label">Texte</label>
                <div class="col-sm-10">
                    <?= $this->Form->textarea('reportmessage',[
                        'label' => 'Texte',
                        'class'=>'form-control',
                        'id'=>'reporttxt',
                        'placeholder'=>'Texte',
                        'required'=>true
                        ]);
                    ?>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <label for="rdvslist" class="col-sm-2 col-form-label">Liste des RDV</label>
                <div class="col-sm-10">
                  <select name="rdv_id" class="form-control" id="rdvslist" required>
                    <option value="">--</option>
                  </select>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <label for="student" class="col-sm-2 col-form-label">Rapport</label>
                <div class="col-sm-10">
                    <?= $this->Form->file('rapport',['id'=>'reportfile', 'required'=>true,'style'=>'display: inline-block']); ?>
                </div>
            </div>
        </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
      <?= $this->Form->end(); ?>
    </div>
  </div>
</div>
<?php endif ?>
<div class="pages">
    <div class="container">
        <h2 style="margin-top: 45px; margin-bottom: 25px;">List des rapports de visites</h2>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Etudiant</th>
                    <th scope="col">Tuteur Pédagogique</th>
                    <th scope="col">Date RDV</th>
                    <th scope="col">Rapport</th>
                    <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0d'): ?>
                        <th>Supprimer</th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
        <?php foreach ($reports as $r):?>
            <tr>
                <td scope="col">
                    <?= $r->student->firstname.' '.$r->student->lastname?>
                </td>
                <td scope="col">
                    <?= ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0d')? $r->ext_tutor->firstname.' '.$r->ext_tutor->lastname : $r->int_tutor->firstname.' '.$r->int_tutor->lastname; ?>
                </td>
                <td scope="col">
                    <?php //print_r($r); die(); ?>
                    <?= $r->rdv->rdv_date->i18nFormat('dd/MM/yyyy').' '.$r->rdv->rdv_time->i18nFormat('HH:mm'); ?>
                </td>
                <td scope="col">
                    <a href="/files/<?= $r->filename;?>"><?= $r->filename;?></a>
                </td>
                <?php if ($loggedIn['role_id'] == '0646b17f-edae-426d-8235-3bbbb0240d0d'): ?>
                    <td><?= $this->Form->postLink(
                                '<i class="fa fa-times"></i>',
                                ['controller' => 'Reports', 'action' => 'delete', $r->id, 'refuse'],
                                ['confirm' => 'Etes vous sur de vouloir supprimer le rapport ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                            ); ?></td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
          </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).on('change','#rdv_student', function(){
    if ($(this).val().length) {
        $.post('/users/studenttutors',{student_id: $(this).val()})
            .done(function(data){
                $('#rdv_tutor').empty();
                $.each(JSON.parse(data),function(i,v){
                    $('#rdv_tutor').append('<option value="" selected>-- Choisissez un Tuteur --</option>');
                    $('#rdv_tutor').append('<option value="'+v.id+'">'+v.firstname+' '+v.lastname+'</option>');
                });
                $('#rdv_tutor').prop('disabled', false);
            });
    }else{
        $('#rdv_tutor').empty();
        $('#rdv_tutor').append('<option value="" selected>--</option>');
        $('#rdv_tutor').prop('disabled', true);
    }
    
})
</script>