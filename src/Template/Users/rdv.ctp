<div class="pages">
    <div class="container">
        <?= $this->Form->create(null, ['url'=>['controller'=>'Users','action'=>'rdvadd']]); ?>
        <div style="display: table;">
            <div class="in-cell">
                <select class="form-control" id="rdv_student" name="student_id" required>
                    <option value="">-- Choisissez un Etudiant --</option>
                    <?php foreach ($students as $student):?>
                        <option value="<?= $student->id; ?>"><?= $student->firstname.' '.$student->lastname?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="in-cell">
                <select name="extutor_id" class="form-control" id="rdv_tutor" disabled>
                    <option>--</option>
                </select>
            </div>
            <div class="in-cell">
                <input type="date" class="form-control" name="rdv_date" required/>
            </div>
            <div class="in-cell">
                <input type="time" class="form-control" name="rdv_time" required/>
            </div>
            <div class="in-cell">
                <input type="text" class="form-control" name="rdv_location" placeholder="Emplacement" required />
            </div>
            <div class="in-cell">
                <button type="submit" class="btn btn-primary" style="margin: 0">Envoyer</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <h2 style="margin-top: 45px; margin-bottom: 25px;">Liste des RDV</h2>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Etudiant</th>
                    <th scope="col">Tuteur Entreprise</th>
                    <th scope="col">Date</th>
                    <th scope="col">Heur</th>
                    <th scope="col">Emplacement</th>
                    <th scope="col">App. par l'Etudiant</th>
                    <th scope="col">App. par le Tuteur</th>
                    <th scope="col">Editer</th>
                    <th scope="col">Annuler</th>
                </tr>
            </thead>
            <tbody>
        <?php foreach ($rdvs as $r):?>
            <tr>
                <td scope="col">
                    <?= $r->student->firstname.' '.$r->student->lastname?>
                </td>
                <td scope="col">
                    <?= $r->ext_tutor->firstname.' '.$r->ext_tutor->lastname?>
                </td>
                <td scope="col">
                    <?= $r->rdv_date->i18nFormat('dd/MM/yyyy'); ?>
                </td>
                <td scope="col">
                    <?= $r->rdv_time->i18nFormat('HH:mm'); ?>
                </td>
                <td scope="col">
                    <?= $r->rdv_location; ?>
                </td>
                <?php if ($r->cancelled == '0') :?>
                <td scope="col" style="color: <?php if ($r->tutor_accept == '1') {echo 'green';}elseif ($r->tutor_accept == '2'){echo 'red';}else{echo 'orange';}?>">
                    <?php switch ($r->student_accept) {
                        case '1':
                            echo 'Oui';
                            break;
                        case '2':
                            echo 'Non';
                            break;
                        default:
                            echo 'En Attente';
                            break;
                    } ?>
                </td>
                <td scope="col">
                    <?php switch ($r->tutor_accept) {
                        case '1':
                            echo '<span style="color: green">Oui</span>';
                            break;
                        case '2':
                            echo '<span style="color: red">Non</span>';
                            break;
                        default:
                            echo '<span style="color: orange">En Attente</span>';
                            break;
                    } ?>
                </td>
                <td>
                    <i class="fa fa-pencil-alt" style="cursor: pointer;" aria-hidden="true" data-toggle="modal" data-target="#rdv<?= $r->id; ?>"></i>
                    <div class="modal fade" id="rdv<?= $r->id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modification du RDV</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <?= $this->Form->create(null,['url'=>['controller'=>'Users','action'=>'rdvupdate']]) ?>
                            <input type="hidden" name="id" value="<?= $r->id;?>">
                            <div class="in-cell">
                                <input type="date" class="form-control" name="rdv_date" value="<?= $r->rdv_date->i18nFormat('yyyy-MM-dd'); ?>" required/>
                            </div>
                            <div class="in-cell">
                                <input type="time" class="form-control" name="rdv_time" value="<?= $r->rdv_time->i18nFormat('HH:mm'); ?>" required/>
                            </div>
                            <div class="in-cell">
                                <input type="text" class="form-control" name="rdv_location" value="<?= $r->rdv_location; ?>" placeholder="Emplacement" required />
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                          </div>
                          <?= $this->Form->end(); ?>
                        </div>
                      </div>
                    </div>
                </td>
                <td>
                    <?= $this->Form->postLink(
                        '<i class="fa fa-ban" aria-hidden="true"></i>',
                        ['controller' => 'Users', 'action' => 'rdvcancel', $r->id],
                        ['confirm' => 'Etes vous sur de vouloir annuler ce RDV?','style'=>'color: red','escape'=>false]
                    ); ?>
                </td>
                <?php else: ?>
                <td colspan="4">
                    <span style="color: red">RDV annule</span>
                </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
          </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).on('change','#rdv_student', function(){
    if ($(this).val().length) {
        $.post('/users/studenttutors',{student_id: $(this).val()})
            .done(function(data){
                $('#rdv_tutor').empty();
                $.each(JSON.parse(data),function(i,v){
                    $('#rdv_tutor').append('<option value="" selected>-- Choisissez un Tuteur --</option>');
                    $('#rdv_tutor').append('<option value="'+v.id+'">'+v.firstname+' '+v.lastname+'</option>');
                });
                $('#rdv_tutor').prop('disabled', false);
            });
    }else{
        $('#rdv_tutor').empty();
        $('#rdv_tutor').append('<option value="" selected>--</option>');
        $('#rdv_tutor').prop('disabled', true);
    }
    
})
</script>