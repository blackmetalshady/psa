<div class="col-md-8" style="margin: auto;">
<?= $this->Form->create(null, ['url'=>['controller'=>'Messages', 'action'=>'add'], 'type' => 'file', 'id'=>'msg-form']) ?>
  <div class="form_details">
  		<div class="form-group">
            <div class="radio" style="display: inline-block; margin-right: 30px;">
                <label>
                    <input type="radio" name='tutors' id="twoTutors" value="0" checked="">Tous les Tuteurs
                </label>
            </div>
            <div class="radio" style="display: inline-block;">
                <label>
                    <input type="radio" name='tutors' id="oneTutor" value="1">Un Tuteur
        </div>
        <div class="form-group">
        <select class="form-control" id="tutorsList" disabled="" name="to_user">
        	<option>(Choisissez un Tuteur)</option>
        	<?php foreach ($user->tutors as $tutor): ?>
        		<option value="<?= $tutor->id ?>"><?= $tutor->firstname.' '.$tutor->lastname.' ('.$tutor->role->name.')' ?></option>
        	<?php endforeach ?>
        </select>
    	</div>
        <div class="form-group">
            <input type="text" class="text" id="subject" name="subject" placeholder="Sujet" required="">
        </div>
        <div class="form-group">
            <textarea placeholder="Votre Message" name="message" id="message" required=""></textarea>
        </div>
	  <div class="clearfix"> </div>
      <div class="form-group">
        <span>Rapport : </span><?= $this->Form->file('rapport',['style'=>'margin-left: 10px;display: inline-block']); ?>
      </div>
	  <div class="clearfix"> </div>
	 <div class="sub-button">
		 <input type="submit" value="Envoyer">
	 </div>
  </div>
  <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    $("input[type=radio]").change(function(){
        if ($(this).val() == '1') {
            $("#tutorsList").attr('disabled', false);
        }else{
            $("#tutorsList").prop('selectedIndex',0);
            $("#tutorsList").attr('disabled', true);
        }
    })
    $("#msg-form").submit(function(e){
        let subject = $("#subject").val();
        let message = $("#message").val();
        let to_user = $("#tutorsList").val();
        $.post('/messages/add',{data: new FormData($("#msg-form")[0])}) //subject:subject, message: message, to_user:to_user
            .done(function(data){
                if (data === 'success') {
                    alert("Votre Message a été envoyé avec succès");
                    $("#msg-form")[0].reset();
                }else{
                    alert("Une Erreur est survenu, veuillez ressayer plutard");
                }
            })
        
        return false;
    });
</script>