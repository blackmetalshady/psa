<!---->
<div class="welcome">
     <div class="container">
         <h2>Bienvenue dans le dispositif de suivi des alternants</h2>
         <div class="welcm_sec">
             <div class="col-md-9 campus">
                 <div class="campus_head">
                     <h3>Bienvenue !</h3>
                     <p>En associant formation académique à l’université et pratique professionnelle en entreprise, l’alternance est une voie des mieux adaptées pour l’obtention simultanée d’un diplôme et d’une insertion professionnelle réussie.
                            Cette relation avec le monde de l’entreprise, dans le cadre d’un contrat de travail, prévoit un tutorat pédagogique et un tutorat professionnel.
                            Le rôle du tuteur pédagogique est de s’assurer que l’étudiant remplit correctement sa mission au sein de l’entreprise et que cette dernière est en adéquation avec les enseignements dispensés par la formation universitaire. Plus précisément, ce tutorat nécessite un suivi régulier qui peut se traduire par des rencontres, des échanges de mails, des appels téléphoniques et surtout des visites en entreprise.
                            Par ailleurs, le tuteur entreprise/maître d’apprentissage aide l’apprenti à s’insérer dans la vie de l’entreprise et le soutient dans la réalisation de sa mission au sein de l’entreprise.
                            La plateforme de suivi de l’alternant est un outil qui a pour objectif de faciliter ce suivi en permettant une évaluation lisible de son projet professionnel.</p>
                 </div>
                 <a href="recommandations.html">Recommandations</a>
                 
             </div>
         </div>
     </div>
</div>
<!---->
<div class="news"></div>
<!---->