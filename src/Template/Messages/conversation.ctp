<?php //print_r($conversation); die() ?>
<div class="container">
<br/>
<div  style="margin: auto;">
	<div>
		<span><?= $this->Html->image('user.png'); ?></span>
		<h3 style="display: inline-block; margin-left: 10px;"><?= $conversation->user->firstname.' '.$conversation->user->lastname; ?></h3>
	</div>
	<div>
		<span style="margin-left: 65px; font-size: 12px">Envoyé Le <?= $conversation->conv_time->i18nFormat('dd/MM/yyyy à HH:mm'); ?></span>
	</div>
	<br/>
	<div><b>Sujet:</b> <?= $conversation->subject ?></div>
</div>
<br/><br/>
<div class="col-md-8" style="margin-left: 20px;">
	<?php foreach ($conversation->messages as $msg): ?>
		<div class="about-head" style="border-bottom: 1px solid #777; padding-bottom: 20px; margin-top: 10px;">
			<span style="font-weight: bold;margin-bottom: 5px; font-size: 12px">Envoyer le <?= $msg->sendtime->i18nFormat('dd/MM/yyyy à HH:mm').' par '.$msg->user->firstname.' '.$msg->user->lastname; ?></span>
			<p><?= $msg->message ?></p>	
			<br/>
			<?php if (!empty($msg->filename)): ?>
				<h6>Pièce jointe: <a href="/files/<?= $msg->filename ?>"><?= $msg->filename ?></a></h6>
			<?php endif ?>
			
		  </div>
	<?php endforeach ?>
</div>
<div class="col-md-8" style="margin-left: 20px;">
<?= $this->Form->create(null, ['url'=>['controller'=>'Messages', 'action'=>'addtoconversation'], 'type' => 'file', 'id'=>'msg-form']) ?>
	  <div class="form_details">
	        <div class="form-group">
	            <textarea placeholder="Votre Message" name="message" id="message" required=""></textarea>
	        </div>
		  <div class="clearfix"> </div>
	      <div class="form-group">
	        <span>Rapport : </span><?= $this->Form->file('rapport',['style'=>'margin-left: 10px;display: inline-block']); ?>
	      </div>
	      <input type="hidden" name="conversation_id" value="<?= $conversation->id;?>">
		  <div class="clearfix"> </div>
		 <div class="sub-button">
			 <input type="submit" value="Envoyer">
		 </div>
	  </div>
  <?= $this->Form->end() ?>
</div>
</div>