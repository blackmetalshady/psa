<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Université de picardie Jules Verne - Plate-form Pédagoguique</title>
<?= $this->Html->css(['bootstrap.css', 'style.css', 'fontawesome/css/all.css']); ?>
<?= $this->Html->script(['jquery.min.js', 'bootstrap.js', 'responsiveslides.min.js', 'custom.js']) ?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

</head>
<body>
<!-- banner -->
<script>  
    $(function () {
      $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
</script>  
<!-- banner --> 
<?php $action = $this->request->getParam('action'); ?>
<div class="banner2">     
     <div class="header">
             <div class="logo">
                 <a href="index.html"><img src="/img/logo.jpg" alt=""/></a>
             </div>
             <div class="top-menu">
                 <span class="menu"></span>
                 <ul class="navig">
                    <li class="<?= ($action === 'display') ? 'active' : '' ?>" ><?= $this->Html->link('Accueil', '/');?></li>
                    <li class="<?= ($action === $rdvaction) ? 'active' : '' ?>">
                        <?php $badge = ($cpt > 0)? '<span class="badge badge-danger">'.$cpt.'</span>':'' ?>
                        <?= $this->Html->link('RDV'.$badge, ['controller'=>'users', 'action'=>$rdvaction],['escape'=>false]) ?></li>
                    <?php if($loggedIn['role_id'] != '0646b17f-edae-426d-8235-3bbbb0240d0e'): ?>
                    <li class="<?= ($action === 'report') ? 'active' : '' ?>" ><?= $this->Html->link('Rapports de Visite', ['controller'=>'users', 'action'=>'reports']);?></li>
                    <?php endif; ?>
                    <li class="<?= ($action === 'inbox') ? 'active' : '' ?>"><?= $this->Html->link('Messagerie', ['controller'=>'users', 'action'=>'inbox']); ?></li>
                    <li class="<?= ($action === 'contact') ? 'active' : '' ?>"><a href="contact.html">Contact</a></li>
                    <?php  if($this->request->getSession()->read('Auth.User')) : ?>
                    <li>
                        <div class="sub-button">
                                <?= $this->Html->link('Déconnexion', ['controller'=>'Users', 'action'=>'logout'])?>
                        </div>
                    </li>
                    <?php endif; ?>
                 </ul>
             </div>
             <!-- script-for-menu -->
         <script>
                $("span.menu").click(function(){
                    $("ul.navig").slideToggle("slow" , function(){
                    });
                });
         </script>
         <!-- script-for-menu -->
             <div class="clearfix"></div>
     </div>   
</div>
<!---->
<?= $this->fetch('content'); ?>
<!---->
<div class="footer">
        <div class="container">
            <div class="ftr-sec">
                <div class="col-md-4 ftr-grid2">
                    <h3>Pages</h3>
                    <ul>
                        <li class="<?= ($action === 'display') ? 'active' : '' ?>" ><?= $this->Html->link('Accueil', '/');?></li>
                    <li class="<?= ($action === $rdvaction) ? 'active' : '' ?>">
                        <?php $badge = ($cpt > 0)? '<span class="badge badge-danger">'.$cpt.'</span>':'' ?>
                        <?= $this->Html->link('RDV'.$badge, ['controller'=>'users', 'action'=>$rdvaction],['escape'=>false]) ?></li>
                    <?php if($loggedIn['role_id'] != '0646b17f-edae-426d-8235-3bbbb0240d0e'): ?>
                    <li class="<?= ($action === 'report') ? 'active' : '' ?>" ><?= $this->Html->link('Rapports de Visite', ['controller'=>'users', 'action'=>'reports']);?></li>
                    <?php endif; ?>
                    <li class="<?= ($action === 'inbox') ? 'active' : '' ?>"><?= $this->Html->link('Messagerie', ['controller'=>'users', 'action'=>'inbox']); ?></li>
                    <li class="<?= ($action === 'contact') ? 'active' : '' ?>"><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
 </div>
<!---->
<div class="copywrite">
        <div class="container">
            <p>Copyright © 2018 Université de Pivardie Jules Verne</p>
        </div>
 </div>
<!---->
</body>
</html>