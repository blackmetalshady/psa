$(function() {

    $('#side-menu').metisMenu();
    $(document).on('click', '.pass-gen', function(){
        generatePassword();
    });

    $(document).on('change', '#checkall', function(){
        if ($(this).is(':checked')) {
            $(".tonextyear").prop( "checked", true );
            $("#passall").prop( "disabled", false );
        }else{
            $(".tonextyear").prop( "checked", false );
            $("#passall").prop( "disabled", true );
        }
    });

    $(document).on('change', '.tonextyear', function(){
        let cpt = 0;
        $(".tonextyear").each(function(){
            if($(this).is(':checked')){
                cpt++;
            }
        });

        if (cpt != 0) {
            $("#passall").prop( "disabled", false );
        }else{
            $("#passall").prop( "disabled", true );
        }
    });
    
    $(document).on('click', '#passall', function(){
        let ids = $(".tonextyear:checked").map(function(){
          return $(this).val();
        }).get();
        if (confirm("ATTENTION vous allez faire passer "+ids.length+" etudiant(s) à l'année suivante, Continuer??")) {
            $.post('/admin/users/nextyear',{ids: ids})
            .done(function(data){
                if (data == "success") {
                    window.location.reload();
                };
            })
        }
    });

    $(document).on('change','#inttutorID, #editintTutorID', function(){
        var tutuorID = $(this).val();
        $('.student').hide();
        $('.std_'+tutuorID).show();
        $.post('/admin/reports/getrdvs',{tutor_id: tutuorID})
            .done(function(data){
                $("#rdvs").html(data);
            });
        $(".studentslist").prop('selectedIndex', 0);
    });

    $(document).on('change','.studentslist', function(){
        var studentID = $(this).val();
        if (studentID) {
            $.post('/admin/reports/getexttutors',{student_id: studentID})
            .done(function(data){
                $("#exttutorID").html(data);
            });
        }else{
            $("#exttutorID").html("<option value=''>--</option>");
            $("#exttutorID").prop('selectedIndex', 0);
        }
        
        //$(".studentslist").prop('selectedIndex', 0);
        
        
    })
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});

function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_&?;*@#",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    $("#pass").val(retVal);
    //console.log(retVal);
    //return retVal;
}