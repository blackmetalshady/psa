$(function() {
    $(document).on('change','#student', function(){
        var student_id = $(this).val();
        if (student_id) {
            $.post('/users/getexttutors',{student_id: student_id})
            .done(function(data){
                $("#exttutor_id").html(data);
            });
        }else{
            $("#exttutor_id").html("<option value=''>--</option>");
            $("#exttutor_id").prop('selectedIndex', 0);
        }
    });

    $(document).on('change','#exttutor_id', function(){
        var student_id = $("#student").val();
        var exttutor_id = $(this).val();
        console.log(student_id);
        if (exttutor_id) {
            $.post('/users/getrdvs',{student_id: student_id, exttutor_id: exttutor_id})
            .done(function(data){
                $("#rdvslist").html(data);
            });
        }else{
            $("#rdvslist").html("<option value=''>--</option>");
            $("#rdvslist").prop('selectedIndex', 0);
        }
    });

})