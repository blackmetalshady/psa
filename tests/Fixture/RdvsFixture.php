<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RdvsFixture
 *
 */
class RdvsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'extutor_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'inttutor_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'student_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'rdv_date' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'rdv_time' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'rdv_location' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'student_accept' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'tutor_accept' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'extutor_id' => ['type' => 'index', 'columns' => ['extutor_id'], 'length' => []],
            'inttutor_id' => ['type' => 'index', 'columns' => ['inttutor_id'], 'length' => []],
            'student_id' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'rdvs_ibfk_1' => ['type' => 'foreign', 'columns' => ['extutor_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'rdvs_ibfk_2' => ['type' => 'foreign', 'columns' => ['inttutor_id'], 'references' => ['rdvs', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
            'rdvs_ibfk_3' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '13cd2024-7678-420f-91f9-ec8ee40fad49',
                'extutor_id' => '535cd0bb-aeac-421e-9b11-3c068913d448',
                'inttutor_id' => '34940728-ac81-4dea-b14c-167e1d84f509',
                'student_id' => '01387f25-7075-4bdf-9f94-d70b685942ae',
                'rdv_date' => '2019-04-09',
                'rdv_time' => '10:54:07',
                'rdv_location' => 'Lorem ipsum dolor sit amet',
                'student_accept' => 1,
                'tutor_accept' => 1
            ],
        ];
        parent::init();
    }
}
