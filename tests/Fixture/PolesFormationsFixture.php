<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PolesFormationsFixture
 *
 */
class PolesFormationsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'pole_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'formation_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'pole_id' => ['type' => 'index', 'columns' => ['pole_id'], 'length' => []],
            'formation_id' => ['type' => 'index', 'columns' => ['formation_id'], 'length' => []],
        ],
        '_constraints' => [
            'poles_formations_ibfk_5' => ['type' => 'foreign', 'columns' => ['pole_id'], 'references' => ['poles', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'poles_formations_ibfk_6' => ['type' => 'foreign', 'columns' => ['formation_id'], 'references' => ['formations', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 'ab2aee27-b587-4c61-bf81-59c8f043a937',
                'pole_id' => '6d7f0e56-4226-4cb7-aeaf-5d22b41e920f',
                'formation_id' => 'b75c13ad-318a-4c3c-aecc-cfe03becf892'
            ],
        ];
        parent::init();
    }
}
