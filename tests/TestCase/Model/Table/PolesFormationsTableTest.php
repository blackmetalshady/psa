<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PolesFormationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PolesFormationsTable Test Case
 */
class PolesFormationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PolesFormationsTable
     */
    public $PolesFormations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PolesFormations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PolesFormations') ? [] : ['className' => PolesFormationsTable::class];
        $this->PolesFormations = TableRegistry::getTableLocator()->get('PolesFormations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PolesFormations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
