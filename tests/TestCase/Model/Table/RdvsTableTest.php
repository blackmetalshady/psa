<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RdvsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RdvsTable Test Case
 */
class RdvsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RdvsTable
     */
    public $Rdvs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Rdvs',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Rdvs') ? [] : ['className' => RdvsTable::class];
        $this->Rdvs = TableRegistry::getTableLocator()->get('Rdvs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rdvs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
