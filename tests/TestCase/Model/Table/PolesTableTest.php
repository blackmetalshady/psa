<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PolesTable Test Case
 */
class PolesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PolesTable
     */
    public $Poles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Poles',
        'app.Users',
        'app.Formations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Poles') ? [] : ['className' => PolesTable::class];
        $this->Poles = TableRegistry::getTableLocator()->get('Poles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Poles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
