<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MessagesActionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MessagesActionsTable Test Case
 */
class MessagesActionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MessagesActionsTable
     */
    public $MessagesActions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MessagesActions',
        'app.Users',
        'app.Messages',
        'app.Conversations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MessagesActions') ? [] : ['className' => MessagesActionsTable::class];
        $this->MessagesActions = TableRegistry::getTableLocator()->get('MessagesActions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MessagesActions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
