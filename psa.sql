-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `psa`;
CREATE DATABASE `psa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psa`;

DROP TABLE IF EXISTS `contracts`;
CREATE TABLE `contracts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `contracts` (`id`, `name`) VALUES
('0646b17f-edae-426d-8235-3bbbb0240d0e',	'Apprentissage'),
('ab5583b2-e657-4632-8ffa-cb4d2db85745',	'Professionnalisation'),
('cc5583b2-e655-4632-8ffa-cb4d2db85668',	'Stage en Alternance');

DROP TABLE IF EXISTS `conversations`;
CREATE TABLE `conversations` (
  `id` char(36) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `conv_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` char(36) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `conversations_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `conversations` (`id`, `subject`, `conv_time`, `user_id`) VALUES
('2fe699f4-2176-4690-9d1b-47a1ee3d1139',	'hello mello',	'2019-05-17 10:22:15',	'ab5583b2-e657-4632-8ffa-cb4d2db85745'),
('346f58aa-89ba-4c30-bde9-356e19641943',	'how to disable WHOIS info',	'2019-03-07 09:02:17',	'ab5583b2-e657-4632-8ffa-cb4d2db85745');

DROP TABLE IF EXISTS `formations`;
CREATE TABLE `formations` (
  `id` char(36) NOT NULL,
  `shortname` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `formations` (`id`, `shortname`, `fullname`) VALUES
('0646b17f-edae-426d-8235-3bbbb0240d0e',	'MIAGE',	'Méthodes informatiques appliquées à la gestion des entreprises'),
('0646b17f-edae-426d-8235-3bbbb0240d0f',	'DEIS',	'Diplôme d\'Etat d\'Ingénierie Sociale'),
('0646b17f-edae-426d-8235-3bbbb0240d1a',	'TC',	'Techniques de Commercialisation'),
('0646b17f-edae-426d-8235-3bbbb0240d1b',	'HSE',	'Hygiène Sécurité et Environnement'),
('0646b17f-edae-426d-8235-3bbbb0240d1c',	'GMP',	'Génie Mécanique et Productique'),
('0646b17f-edae-426d-8235-3bbbb0240d1d',	'GC',	'Génie Civil'),
('0646b17f-edae-426d-8235-3bbbb0240d2a',	'GEA - AS',	'Gestion des Entreprises et des Administrations en Année Spéciale');

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` char(36) CHARACTER SET utf8 NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `to_user` char(36) CHARACTER SET utf8 DEFAULT NULL,
  `sendtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text CHARACTER SET utf8 NOT NULL,
  `filename` text CHARACTER SET utf8,
  `conversation_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `to` (`to_user`),
  KEY `id` (`id`),
  KEY `conversation_id` (`conversation_id`),
  CONSTRAINT `messages_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_5` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_7` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `messages` (`id`, `user_id`, `to_user`, `sendtime`, `message`, `filename`, `conversation_id`) VALUES
('5b67f81c-da75-4e02-aa4b-eae95622ab71',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	NULL,	'2019-05-17 11:47:50',	'zrszgdfhjgfj',	'TD - Algèbre de Bool.pdf_1558093670.pdf',	'2fe699f4-2176-4690-9d1b-47a1ee3d1139'),
('60f9c796-de92-4523-8989-b194f47d503f',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	NULL,	'2019-05-17 11:47:53',	'zrszgdfhjgfj',	'TD - Algèbre de Bool.pdf_1558093673.pdf',	'2fe699f4-2176-4690-9d1b-47a1ee3d1139'),
('8afab9a9-1b7e-4586-97d9-17c29d37068b',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	NULL,	'2019-05-17 12:00:55',	'Hello World',	'TP conversion.docx_1558094455.docx',	'2fe699f4-2176-4690-9d1b-47a1ee3d1139'),
('c086278a-3e54-4671-8599-b08f9e8f5c39',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	'278acd65-6ce3-4c99-93fe-75f5deecab5c',	'2019-03-07 09:02:17',	'fgnj,gfhgh,gh',	'CBI19_paper_68.pdf_1551949337.pdf',	'346f58aa-89ba-4c30-bde9-356e19641943');

DROP TABLE IF EXISTS `messages_actions`;
CREATE TABLE `messages_actions` (
  `id` char(36) NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `messages_id` char(36) CHARACTER SET utf8 DEFAULT NULL,
  `conversation_id` char(36) DEFAULT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `action_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `messages_id` (`messages_id`),
  CONSTRAINT `messages_actions_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `messages_actions_ibfk_5` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `messages_actions_ibfk_6` FOREIGN KEY (`messages_id`) REFERENCES `messages` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `poles`;
CREATE TABLE `poles` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `admin_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `poles_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `poles` (`id`, `name`, `admin_id`) VALUES
('288e9dcf-c485-4931-982e-72409ae36bde',	'Pole 3',	'ab5583b2-e655-4632-8ffa-cb4d2db85662'),
('cefa7f71-c860-43ec-a6ad-7c383836de63',	'Pole 1',	'ab5583b2-e655-4632-8ffa-cb4d2db85662'),
('d359b3ad-d58d-446b-b79d-c0c4d6ec51d7',	'Pole 2',	'ab5583b2-e655-4632-8ffa-cb4d2db85662');

DROP TABLE IF EXISTS `poles_formations`;
CREATE TABLE `poles_formations` (
  `id` char(36) NOT NULL,
  `pole_id` char(36) NOT NULL,
  `formation_id` char(36) CHARACTER SET latin1 NOT NULL,
  KEY `pole_id` (`pole_id`),
  KEY `formation_id` (`formation_id`),
  CONSTRAINT `poles_formations_ibfk_5` FOREIGN KEY (`pole_id`) REFERENCES `poles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `poles_formations_ibfk_6` FOREIGN KEY (`formation_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `poles_formations` (`id`, `pole_id`, `formation_id`) VALUES
('288e9dcf-c485-4931-982e-72409ae36ea5',	'288e9dcf-c485-4931-982e-72409ae36bde',	'0646b17f-edae-426d-8235-3bbbb0240d0e'),
('188e9dcf-c485-4931-982e-72409ae36ea5',	'288e9dcf-c485-4931-982e-72409ae36bde',	'0646b17f-edae-426d-8235-3bbbb0240d2a'),
('17348daf-8d1c-4ae2-b526-06e80f35d57b',	'cefa7f71-c860-43ec-a6ad-7c383836de63',	'0646b17f-edae-426d-8235-3bbbb0240d0f'),
('863c3714-06db-44ef-bc1c-273c78c0da4e',	'cefa7f71-c860-43ec-a6ad-7c383836de63',	'0646b17f-edae-426d-8235-3bbbb0240d1a'),
('0240a5bd-413c-4aab-acbb-7db404fed598',	'd359b3ad-d58d-446b-b79d-c0c4d6ec51d7',	'0646b17f-edae-426d-8235-3bbbb0240d1d'),
('7b6525d8-fafb-4dc9-bae7-ca84bec7a305',	'd359b3ad-d58d-446b-b79d-c0c4d6ec51d7',	'0646b17f-edae-426d-8235-3bbbb0240d1c'),
('2382823e-1ab5-4226-93c6-63fc1fc4defd',	'd359b3ad-d58d-446b-b79d-c0c4d6ec51d7',	'0646b17f-edae-426d-8235-3bbbb0240d1b');

DROP TABLE IF EXISTS `rdvs`;
CREATE TABLE `rdvs` (
  `id` char(36) NOT NULL,
  `extutor_id` char(36) NOT NULL,
  `inttutor_id` char(36) NOT NULL,
  `student_id` char(36) NOT NULL,
  `rdv_date` date NOT NULL,
  `rdv_time` time NOT NULL,
  `rdv_location` varchar(255) NOT NULL,
  `student_accept` tinyint(2) NOT NULL DEFAULT '0',
  `tutor_accept` tinyint(2) NOT NULL DEFAULT '0',
  `cancelled` tinyint(2) NOT NULL DEFAULT '0',
  `report_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `extutor_id` (`extutor_id`),
  KEY `student_id` (`student_id`),
  KEY `inttutor_id` (`inttutor_id`),
  KEY `report_id` (`report_id`),
  CONSTRAINT `rdvs_ibfk_1` FOREIGN KEY (`extutor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `rdvs_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `rdvs_ibfk_4` FOREIGN KEY (`inttutor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `rdvs_ibfk_7` FOREIGN KEY (`report_id`) REFERENCES `reports` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `rdvs` (`id`, `extutor_id`, `inttutor_id`, `student_id`, `rdv_date`, `rdv_time`, `rdv_location`, `student_accept`, `tutor_accept`, `cancelled`, `report_id`) VALUES
('fce7b879-827f-43bd-8a3b-11d492daa53a',	'5774a025-a59f-40d6-85f5-05470cfcdd76',	'278acd65-6ce3-4c99-93fe-75f5deecab5c',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	'2019-05-14',	'15:00:00',	'Salle de présentation',	1,	1,	0,	'c61449f4-dbac-44f8-82f8-b72eb88a6ef8');

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` char(36) NOT NULL,
  `exttutor_id` char(36) NOT NULL,
  `inttutor_id` char(36) NOT NULL,
  `student_id` char(36) NOT NULL,
  `upload_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reportmessage` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tutor_id` (`exttutor_id`),
  KEY `student_id` (`student_id`),
  KEY `inttutor_id` (`inttutor_id`),
  CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`exttutor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `reports_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `reports_ibfk_3` FOREIGN KEY (`inttutor_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `reports` (`id`, `exttutor_id`, `inttutor_id`, `student_id`, `upload_time`, `reportmessage`, `filename`) VALUES
('c61449f4-dbac-44f8-82f8-b72eb88a6ef8',	'5774a025-a59f-40d6-85f5-05470cfcdd76',	'278acd65-6ce3-4c99-93fe-75f5deecab5c',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	'2019-05-16 14:54:44',	'-y_çy_çy',	'TP conversion.docx_1558018484.docx');

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`id`, `name`) VALUES
('0646b17f-edae-426d-8235-3bbbb0240d0a',	'Admin Pôle'),
('0646b17f-edae-426d-8235-3bbbb0240d0b',	'Admin de l’Université'),
('0646b17f-edae-426d-8235-3bbbb0240d0c',	'Tuteur Entreprise'),
('0646b17f-edae-426d-8235-3bbbb0240d0d',	'Tuteur Pédagogique'),
('0646b17f-edae-426d-8235-3bbbb0240d0e',	'Etudiant'),
('0646b17f-edae-426d-8235-3bbbb0240d0f',	'Admin de Formation');

DROP TABLE IF EXISTS `students_tutors`;
CREATE TABLE `students_tutors` (
  `id` char(36) CHARACTER SET utf8 NOT NULL,
  `student_id` char(36) CHARACTER SET utf8 NOT NULL,
  `tutor_id` char(36) CHARACTER SET utf8 NOT NULL,
  `date_affectation` date NOT NULL,
  `mission` text NOT NULL,
  `current_tutor` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `tutor_id` (`tutor_id`),
  CONSTRAINT `students_tutors_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `students_tutors_ibfk_2` FOREIGN KEY (`tutor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `students_tutors` (`id`, `student_id`, `tutor_id`, `date_affectation`, `mission`, `current_tutor`) VALUES
('23a9034d-d332-4526-874f-3557425618e9',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	'5774a025-a59f-40d6-85f5-05470cfcdd76',	'2019-03-12',	'Hello World!!',	1),
('2efbd1f9-6125-413c-ad0c-1bed59f7e37a',	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	'278acd65-6ce3-4c99-93fe-75f5deecab5c',	'2019-04-09',	'is this the real life',	1),
('665bd3df-8495-4865-9a36-4f3759bcc833',	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	'5774a025-a59f-40d6-85f5-05470cfcdd76',	'2019-05-22',	'sfdqsfqdf',	0),
('b74944a3-ae30-473d-8bfe-0329f6750aee',	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	'f129e1a3-708d-4493-bf6a-2cdbafa17837',	'2019-05-22',	'ezefr',	0);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `role_id` char(36) NOT NULL DEFAULT '0646b17f-edae-426d-8235-3bbbb0240d0d',
  `has_both_roles` tinyint(4) NOT NULL DEFAULT '0',
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` tinytext NOT NULL,
  `phone` varchar(20) NOT NULL,
  `company_address` tinytext,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `confirmed` tinyint(4) NOT NULL DEFAULT '1',
  `formation_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `contract_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `formation_id` (`formation_id`),
  KEY `contract_id` (`contract_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`formation_id`) REFERENCES `formations` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_5` FOREIGN KEY (`contract_id`) REFERENCES `contracts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `role_id`, `has_both_roles`, `firstname`, `lastname`, `email`, `address`, `phone`, `company_address`, `password`, `created`, `last_login`, `enabled`, `confirmed`, `formation_id`, `contract_id`) VALUES
('23ae1569-0b74-45f0-a1f2-0a088e1e25df',	'0646b17f-edae-426d-8235-3bbbb0240d0e',	0,	'Brad',	'Pitt',	'brad.pitt@gmail.com',	'Hollywood, USA',	'675342668',	'No Where',	'$2y$10$BLEhs6Q5NW5d/IW3kcYpgehxAdh1vywlCtdoy.uY/iTdzuRdQIvLS',	'2019-03-11 11:12:33',	NULL,	1,	1,	'0646b17f-edae-426d-8235-3bbbb0240d0e',	'cc5583b2-e655-4632-8ffa-cb4d2db85668'),
('278acd65-6ce3-4c99-93fe-75f5deecab5c',	'0646b17f-edae-426d-8235-3bbbb0240d0d',	0,	'John',	'Doe',	'yas-yo2@hotmail.com',	'John Doe\'s Address',	'+3387412369',	NULL,	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-02-24 12:45:25',	NULL,	1,	1,	NULL,	NULL),
('4d09b678-680b-45c6-8d7a-9b8d516742c5',	'0646b17f-edae-426d-8235-3bbbb0240d0e',	0,	'Freddie',	'Mercury',	'freddie.mercury@gmail.com',	'Laboratory of Information Technology and Modeling, Faculty of Science Ben M’Sik',	'675342668',	'Laboratory of Information Technology and Modeling, Faculty of Science Ben M’Sik',	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-03-11 09:44:57',	NULL,	1,	1,	'0646b17f-edae-426d-8235-3bbbb0240d0f',	NULL),
('5774a025-a59f-40d6-85f5-05470cfcdd76',	'0646b17f-edae-426d-8235-3bbbb0240d0c',	0,	'John',	'Wick',	'yas-yo@hotmail.com',	'My Address',	'+33965478963',	NULL,	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-02-24 12:08:55',	NULL,	1,	1,	NULL,	NULL),
('ab5583b2-e655-4632-8ffa-cb4d2db85553',	'0646b17f-edae-426d-8235-3bbbb0240d0f',	0,	'admin',	'formation',	'admin.formation@gmail.com',	'Admin Formation Address',	'789654123',	NULL,	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-05-20 10:00:23',	'2019-05-20 10:00:23',	1,	1,	NULL,	NULL),
('ab5583b2-e655-4632-8ffa-cb4d2db85662',	'0646b17f-edae-426d-8235-3bbbb0240d0a',	0,	'admin',	'pole',	'admin.pole@gmail.com',	'Admin Pole Address',	'159632478',	NULL,	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-05-20 09:58:40',	'2019-05-20 09:58:40',	1,	1,	NULL,	NULL),
('ab5583b2-e655-4632-8ffa-cb4d2db85771',	'0646b17f-edae-426d-8235-3bbbb0240d0b',	0,	'Maria',	'Sekkouri',	'maria.sekkouri@gmail.com',	'Address Line 1',	'',	'Address Line 2',	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-01-31 11:08:20',	NULL,	1,	1,	NULL,	NULL),
('ab5583b2-e657-4632-8ffa-cb4d2db85745',	'0646b17f-edae-426d-8235-3bbbb0240d0e',	0,	'Morgan',	'Freeman',	'morgan.freeman@gmail.com',	'',	'',	'',	'$2y$10$xGVDOo6RuLCM3JUfuVBeWebUjOsuA8pHLSTuuAvWLY9N8.bJ8K3JK',	'2019-01-30 11:31:12',	NULL,	1,	1,	'0646b17f-edae-426d-8235-3bbbb0240d0e',	NULL),
('f129e1a3-708d-4493-bf6a-2cdbafa17837',	'0646b17f-edae-426d-8235-3bbbb0240d0d',	0,	'test',	'best',	'test.best@gmail.comx',	'sdfsdfs',	'675342668',	NULL,	'$2y$10$rw3T1Z7bcGTzrJ2igtPaj.m72hScsE9a07wcjo.oy4NWND7g6ljb6',	'2019-05-17 14:54:03',	NULL,	1,	1,	NULL,	NULL);

DROP TABLE IF EXISTS `years`;
CREATE TABLE `years` (
  `id` char(36) NOT NULL,
  `syear` int(11) NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `current_year` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `years_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `years` (`id`, `syear`, `user_id`, `current_year`) VALUES
('020eedd2-339e-498e-bd78-55b3a27f51e5',	2021,	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	0),
('0bca021e-3c98-4304-a287-c296dbadb7aa',	2019,	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	0),
('1ff260be-5cab-4c27-bec8-3dfb1fb42e3b',	2019,	'4d09b678-680b-45c6-8d7a-9b8d516742c5',	0),
('1ff260be-5cab-4c27-bec8-3dfb1fb42e3c',	2018,	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	0),
('2cf466d6-daef-493a-ba38-d62a62130eb4',	2020,	'4d09b678-680b-45c6-8d7a-9b8d516742c5',	0),
('389007f5-1915-4681-94fb-1e56ce5ccb32',	2019,	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	0),
('4467e151-0417-45b5-b81f-67435f91a592',	2022,	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	1),
('5ecf3d9f-32a3-4053-acf0-8708bdf30a20',	2020,	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	0),
('8ffef13b-29d3-48d2-b49d-bf11aa3a3212',	2020,	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	0),
('9af912d1-edec-4b35-b60a-44bc1b1da27a',	2021,	'4d09b678-680b-45c6-8d7a-9b8d516742c5',	0),
('a4ecba3e-9808-407c-9ba2-07cdfad629c9',	2022,	'ab5583b2-e657-4632-8ffa-cb4d2db85745',	1),
('a51bb9cc-b8f2-4bcd-a7a9-d88437746162',	2022,	'4d09b678-680b-45c6-8d7a-9b8d516742c5',	1),
('acb1b3fc-c0a8-4639-947e-3434673d724d',	2021,	'23ae1569-0b74-45f0-a1f2-0a088e1e25df',	0);

-- 2019-05-22 11:20:52
